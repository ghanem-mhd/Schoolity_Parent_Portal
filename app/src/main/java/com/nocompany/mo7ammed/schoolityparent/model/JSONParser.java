package com.nocompany.mo7ammed.schoolityparent.model;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class JSONParser {
    public static List<Comment> getCommentsList(JSONArray jsonArray) {
        Gson gson = new Gson();
        Type listType = new TypeToken<List<Comment>>() {
        }.getType();
        return gson.fromJson(jsonArray.toString(), listType);
    }

    public static ArrayList<Feedable> parseFeed(JSONArray jsonArray){
        Gson gson                       = new Gson();
        Type listType                   = new TypeToken<List<FeedItem>>() {}.getType();
        List<FeedItem> feedItems        = gson.fromJson(jsonArray.toString(), listType);
        ArrayList<Feedable> feedables   = new ArrayList<>();
        for (FeedItem feedItem:feedItems) {
            String entryJson = gson.toJson(feedItem.entry);

            if (feedItem.type.equals("Announcement")){
                Announcement announcement = gson.fromJson(entryJson,Announcement.class);
                feedables.add(announcement);
            }

            if (feedItem.type.equals("Grade")){
                Grade grade = gson.fromJson(entryJson,Grade.class);
                feedables.add(grade);
            }
        }

        return feedables;
    }

    public static FollowedStudent newFollowedStudent(String jsonObjectString) {
        try {
            JSONObject jsonObject = new JSONObject(jsonObjectString);
            return newFollowedStudent(jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static FollowedStudent newFollowedStudent(JSONObject jsonObject){
        Gson gson = new Gson();
        return gson.fromJson(jsonObject.toString(),FollowedStudent.class);
    }

    public static List<FollowedStudent> getFollowedStudentsFromJsonArray(JSONArray jsonArray) {
        Gson gson = new Gson();
        Type listType = new TypeToken<List<FollowedStudent>>() {
        }.getType();
        return gson.fromJson(jsonArray.toString(), listType);
    }

    public static List<Grade> getGradeList(JSONArray jsonArray) {
        Gson gson = new Gson();
        Type listType = new TypeToken<List<Grade>>() {
        }.getType();
        return gson.fromJson(jsonArray.toString(), listType);
    }

    public static Timetable newTimetable(JSONObject jsonObject) {
        Gson gson = new Gson();
        return gson.fromJson(jsonObject.toString(), Timetable.class);
    }

    public static UserInfo newUserInfo(JSONObject jsonObject) {
        Gson gson = new Gson();
        return gson.fromJson(jsonObject.toString(), UserInfo.class);
    }

    public static List<Announcement> getAnnouncementsList(JSONArray jsonArray) {
        Gson gson = new Gson();
        Type listType = new TypeToken<List<Announcement>>() {
        }.getType();
        return gson.fromJson(jsonArray.toString(), listType);
    }

    public static AgendaDay getNewAgendaDay(JSONObject jsonObject) {
        Gson gson = new Gson();
        return gson.fromJson(jsonObject.toString(),AgendaDay.class);
    }

    public static List<Absence> getAbsencesList(JSONArray jsonArray) {
        Gson gson = new Gson();
        Type listType = new TypeToken<List<Absence>>() {
        }.getType();
        return gson.fromJson(jsonArray.toString(), listType);
    }

    public static List<Message> getMessageList(JSONArray jsonArray) {
        Gson gson = new Gson();
        Type listType = new TypeToken<List<Message>>() {}.getType();
        List<Message> messages =  gson.fromJson(jsonArray.toString(), listType);

        for (Message message:messages){
            String entryJson = gson.toJson(message.getContent());

            if (message.getContent_type().equals("absence")){
                Message.AbsenceMessageContent absenceMessageContent = gson.fromJson(entryJson,Message.AbsenceMessageContent.class);
                message.setContent(absenceMessageContent);
            }

            if (message.getContent_type().equals("basic")){
                Message.BasicMessageContent basicMessageContent = gson.fromJson(entryJson,Message.BasicMessageContent.class);
                message.setContent(basicMessageContent);
            }
        }
        return messages;
    }
}
