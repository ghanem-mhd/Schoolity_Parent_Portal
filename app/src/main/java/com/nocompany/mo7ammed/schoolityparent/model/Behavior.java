package com.nocompany.mo7ammed.schoolityparent.model;

import java.io.Serializable;

public class Behavior implements Serializable {

    private int id;
    private String content_type;
    private BehaviorRatedContent content;
    private int comments_count;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent_type() {
        return content_type;
    }

    public void setContent_type(String content_type) {
        this.content_type = content_type;
    }

    public BehaviorRatedContent getContent() {
        return content;
    }

    public void setContent(BehaviorRatedContent content) {
        this.content = content;
    }

    public int getComments_count() {
        return comments_count;
    }

    public void setComments_count(int comments_count) {
        this.comments_count = comments_count;
    }

    @Override
    public String toString() {
        return "Behavior{" +
                "content_type='" + content_type + '\'' +
                ", content=" + content +
                ", comments_count=" + comments_count +
                '}';
    }
}
