package com.nocompany.mo7ammed.schoolityparent.main_ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.nocompany.mo7ammed.schoolityparent.R;
import com.nocompany.mo7ammed.schoolityparent.api.GradesRequest;
import com.nocompany.mo7ammed.schoolityparent.api.VolleyHandler;
import com.nocompany.mo7ammed.schoolityparent.helper.SimpleDividerItemDecoration;
import com.nocompany.mo7ammed.schoolityparent.model.FollowedStudent;
import com.nocompany.mo7ammed.schoolityparent.model.Grade;
import com.nocompany.mo7ammed.schoolityparent.model.JSONParser;
import com.nocompany.mo7ammed.schoolityparent.ui_adapter.GradesAdapter;
import com.nocompany.mo7ammed.schoolityparent.utilities.BaseFragment;
import com.nocompany.mo7ammed.schoolityparent.utilities.StudentMenuManager;

import org.json.JSONArray;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class GradesFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, GradesAdapter.GradesAdapterInterface, StudentMenuManager.StudentMenuInterface {

    @BindView(R.id.grades_recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.grades_swipeRefresh)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.grades_error_textView)
    TextView errorTextView;

    @BindView(R.id.grades_error_scrollView)
    ScrollView scrollView;

    private GradesAdapter gradesAdapter;

    private StudentMenuManager studentMenuManager;

    private FollowedStudent currentSelectedStudent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        studentMenuManager = new StudentMenuManager(this, getString(R.string.grades));
        studentMenuManager.setStudentMenuInterface(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.grades_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUpAdsList();
        studentMenuManager.getFollowingStudent();
    }

    private void setUpAdsList() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        gradesAdapter = new GradesAdapter();

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(gradesAdapter);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity().getApplicationContext()));

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getActivity(), R.color.colorAccent));

        gradesAdapter.setGradesAdapterInterface(this);
    }

    private void API_Grades_Request(boolean fromCache) {
        if (currentSelectedStudent == null){
            setRefreshing(false);
            return;
        }
        setRefreshing(true);
        GradesRequest gradesRequest = new GradesRequest(getActivity(), currentSelectedStudent.getFollowingID()) {
            @Override
            protected void doOnResponse(JSONArray jsonArray) {
                API_Grades_Response(jsonArray);
            }

            @Override
            protected void doOnError(VolleyError error) {
                VolleyHandler.handleVolleyError(error);
                API_Grades_Error(error);
            }
        };
        gradesRequest.request(fromCache);
    }

    private void API_Grades_Response(JSONArray jsonArray) {
        setRefreshing(false);
        List<Grade> grades = JSONParser.getGradeList(jsonArray);
        if (grades != null && grades.size() > 0) {
            scrollView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            gradesAdapter.setNewData(grades);
            recyclerView.swapAdapter(gradesAdapter, true);
        } else {
            showWarningTextView(getString(R.string.no_grades));
        }
    }

    private void API_Grades_Error(VolleyError volleyError) {
        setRefreshing(false);
        On_API_Error(volleyError, new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void showWarningTextView(String warningMessage) {
        recyclerView.setVisibility(View.GONE);
        scrollView.setVisibility(View.VISIBLE);
        errorTextView.setText(warningMessage);
    }


    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                API_Grades_Request(false);
            }
        }, 500);
    }

    @Override
    public void onGradeClick(Grade grade, int position) {
        Intent intent = new Intent(getActivity(), GradeViewerActivity.class);
        intent.putExtra(GradeViewerActivity.Grade_OBJECT_ARG, grade);
        startActivity(intent);
    }

    @Override
    public void onGradeLongPress(View view, Grade followedStudent, int position) {

    }

    private void setRefreshing(final boolean isRefreshing) {
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(isRefreshing);
            }
        });
    }


    @Override
    public void onChangeStudent(FollowedStudent followedStudent) {
        currentSelectedStudent = followedStudent;
        API_Grades_Request(true);
    }

    @Override
    public void onEmptyGetStudents() {
        setToolbarTitle();
        showWarningTextView(getString(R.string.no_following_student_go));
    }

    private void setToolbarTitle() {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.main_toolbar);
        toolbar.setTitle(getString(R.string.grades));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        studentMenuManager.clearMenuAndSave();
    }
}
