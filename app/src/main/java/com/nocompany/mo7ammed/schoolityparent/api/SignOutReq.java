package com.nocompany.mo7ammed.schoolityparent.api;

import android.content.Context;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.nocompany.mo7ammed.schoolityparent.BuildConfig;

public abstract class SignOutReq extends Request {
    private static final String TAG = "SignOutReq";
    private static final int METHOD = Request.DELETE;
    private static final String URI = Request.REST_SERVER + "sign_out";
    private Response.Listener<String> stringListener = new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {
            SignOutReq.this.doOnResponse(response);
        }
    };

    private Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            SignOutReq.this.doOnError(error);
        }
    };


    public SignOutReq(Context context) {
        this.isAuthRequest = true;
        conHandler = new VolleyHandler(context);
    }


    @Override
    public void request(boolean withCaching) {
        CustomStringRequest customStringRequest = new CustomStringRequest(
                METHOD,
                URI,
                stringListener,
                errorListener,
                isAuthRequest);

        if (BuildConfig.DEBUG)
            Log.d(TAG, "making a request Method:" + METHOD + " URI:" + URI);

        conHandler.connect(customStringRequest);
    }

    protected abstract void doOnResponse(String response);
}
