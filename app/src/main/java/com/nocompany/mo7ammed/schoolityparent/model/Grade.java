package com.nocompany.mo7ammed.schoolityparent.model;


import android.content.Context;

import com.nocompany.mo7ammed.schoolityparent.R;

import java.io.Serializable;

public class Grade implements Serializable,Feedable {

    private int id;
    private String score;
    private Exam exam;
    private String student;
    private int comments_count;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public Exam getExam() {
        return exam;
    }

    public void setExam(Exam exam) {
        this.exam = exam;
    }

    @Override
    public String toString() {
        return "Grade{" +
                "id=" + id +
                ", score='" + score + '\'' +
                ", exam=" + exam +
                ", student='" + student + '\'' +
                ", comments_count=" + comments_count +
                '}';
    }

    public int getComments_count() {
        return comments_count;
    }

    public void setComments_count(int comments_count) {
        this.comments_count = comments_count;
    }

    @Override
    public String getType(Context context) {
        return context.getString(R.string.grade);
    }

    @Override
    public String getTitle(Context context) {
        return exam.getSubject().getName()+" "+context.getString(R.string.grade);
    }

    @Override
    public String getSummary(Context context) {
        return exam.getDate();
    }

    public String getStudent() {
        return student;
    }

    public void setStudent(String student) {
        this.student = student;
    }
}
