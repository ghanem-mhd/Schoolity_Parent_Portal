package com.nocompany.mo7ammed.schoolityparent.api;

import android.content.Context;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.nocompany.mo7ammed.schoolityparent.BuildConfig;

import org.json.JSONException;
import org.json.JSONObject;

public abstract class RegisterGCMToken extends Request {
    private static final String TAG = "SignInRequest";
    private static final String URI = Request.REST_SERVER + "/parent/tokens";
    private static final int METHOD = Request.POST;
    private JSONObject requestBody;
    private Response.Listener<JSONObject> jsonObjectListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            RegisterGCMToken.this.doOnResponse(response);
        }
    };
    private Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            RegisterGCMToken.this.doOnError(error);
        }
    };

    public RegisterGCMToken(Context context,String token) {
        this.isAuthRequest = true;
        conHandler = new VolleyHandler(context);
        requestBody = getJsonObjectBody(token);
    }

    @Override
    public void request(boolean withCaching) {
        CustomJsonObjectRequest customJsonObjectRequest = new CustomJsonObjectRequest(
                METHOD,
                URI,
                requestBody,
                jsonObjectListener,
                errorListener,
                isAuthRequest
        );

        if (BuildConfig.DEBUG)
            Log.d(TAG, "making a request Method:" + METHOD + " URI:" + URI + " BODY:" + requestBody);
        conHandler.connect(customJsonObjectRequest);
    }

    private JSONObject getJsonObjectBody(String token) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("token",token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    protected abstract void doOnResponse(JSONObject jsonObject);
}
