package com.nocompany.mo7ammed.schoolityparent.api;


import android.content.Context;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.nocompany.mo7ammed.schoolityparent.BuildConfig;
import com.nocompany.mo7ammed.schoolityparent.db_cache.JSONsDatabaseHelper;
import com.nocompany.mo7ammed.schoolityparent.model.JSON;

import org.json.JSONObject;

public abstract class AgendaRequest extends Request {
    private static final String TAG = "AgendaRequest";
    private static final int METHOD = Request.GET;
    JSONsDatabaseHelper jsoNsDatabaseHelper;
    private String URI = Request.REST_SERVER + "parent/followings/%s/agendas/%s-%s-%s";
    private Response.Listener<JSONObject> jsonObjectListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            jsoNsDatabaseHelper.addJSON(new JSON(URI, response.toString()));
            AgendaRequest.this.doOnResponse(response);
        }
    };

    private Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            AgendaRequest.this.doOnError(error);
        }
    };

    public AgendaRequest(Context context, int followingID, int year, int month, int day) {
        this.isAuthRequest = true;
        URI = String.format(URI, followingID, year, month, day);
        conHandler = new VolleyHandler(context);
        jsoNsDatabaseHelper = JSONsDatabaseHelper.getInstance();
    }

    @Override
    public void request(boolean withCaching) {

        if (withCaching) {
            checkCache();
        }

        CustomJsonObjectRequest customJsonObjectRequest = new CustomJsonObjectRequest(
                METHOD,
                URI,
                null,
                jsonObjectListener,
                errorListener,
                isAuthRequest);

        if (BuildConfig.DEBUG)
            Log.d(TAG, "making a request Method:" + METHOD + " URI:" + URI);

        conHandler.connect(customJsonObjectRequest);
    }

    private void checkCache() {
        JSON json = jsoNsDatabaseHelper.getJSON(URI);
        if (json != null) {
            AgendaRequest.this.doOnResponse(json.getJsonObject());
        } else {
            if (BuildConfig.DEBUG)
                Log.d(TAG, " Agenda Not Found in cache");
        }
    }

    protected abstract void doOnResponse(JSONObject jsonObject);

}
