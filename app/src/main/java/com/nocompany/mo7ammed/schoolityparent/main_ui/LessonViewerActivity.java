package com.nocompany.mo7ammed.schoolityparent.main_ui;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.nocompany.mo7ammed.schoolityparent.R;
import com.nocompany.mo7ammed.schoolityparent.model.Activity;
import com.nocompany.mo7ammed.schoolityparent.model.BasicLessonContent;
import com.nocompany.mo7ammed.schoolityparent.model.Behavior;
import com.nocompany.mo7ammed.schoolityparent.model.Classroom;
import com.nocompany.mo7ammed.schoolityparent.model.Lesson;
import com.nocompany.mo7ammed.schoolityparent.utilities.BaseActivity;
import com.nocompany.mo7ammed.schoolityparent.utilities.Constant;

public class LessonViewerActivity extends BaseActivity{

    public static final String CLASSROOM_OBJECT_ARG    = "classroomObject";
    public static final String LESSON_OBJECT_ARG        = "lessonObject";
    public static final String STUDENT_NAME_ARG         = "studentName";

    private String studentNameString;
    private Lesson lesson;
    private Classroom classroom;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lesson_viewer_activity);

        lesson              = (Lesson) getIntent().getSerializableExtra(LESSON_OBJECT_ARG);
        studentNameString   = getIntent().getStringExtra(STUDENT_NAME_ARG);
        classroom           = (Classroom) getIntent().getSerializableExtra(CLASSROOM_OBJECT_ARG);
        setUpToolbar();

        setLessonToUI();

        setClassRoomToUI();
    }

    private void setActivitiesToUI() {

        if (lesson.getActivity() != null){
            TextView homeworkRatingTV       = (TextView) findViewById(R.id.lesson_viewer_homework_rate);
            TextView participationRatingTV  = (TextView) findViewById(R.id.lesson_viewer_participation_rate);
            TextView assessmentRatingTV     = (TextView) findViewById(R.id.lesson_viewer_assessment_rate);
            TextView notesTV                = (TextView) findViewById(R.id.lesson_viewer_activities_notes);

            assert homeworkRatingTV        != null;
            assert participationRatingTV   != null;
            assert assessmentRatingTV      != null;
            assert notesTV                 != null;

            Activity activity = lesson.getActivity();
            //TODO color rating ...
            homeworkRatingTV.setText(activity.getContent().getHomework());
            participationRatingTV.setText(activity.getContent().getParticipation());
            assessmentRatingTV.setText(activity.getContent().getAssessment());
            notesTV.setText(activity.getContent().getNotes());
        }
    }


    private void setLessonToUI() {

        TextView orderTV      = (TextView) findViewById(R.id.lesson_viewer_order);
        TextView subjectTV    = (TextView) findViewById(R.id.lesson_viewer_subject);
        TextView titleTV      = (TextView) findViewById(R.id.lesson_viewer_title);
        TextView summaryTV    = (TextView) findViewById(R.id.lesson_viewer_summary);
        TextView homeworkTV   = (TextView) findViewById(R.id.lesson_viewer_homework);

        String lessonOrderString = "#%s %s";
        assert orderTV      != null;
        assert subjectTV    != null;
        assert titleTV      != null;
        assert summaryTV    != null;
        assert homeworkTV   != null;

        orderTV.setText(String.format(lessonOrderString,lesson.getOrder(),getString(R.string.lesson_keyword)));
        subjectTV.setText(lesson.getSubject());

        BasicLessonContent basicLessonContent = lesson.getContent();
        if (!basicLessonContent.getTitle().isEmpty()){
            titleTV.setText(lesson.getContent().getTitle());
        }

        if (!basicLessonContent.getSummary().isEmpty()){
            summaryTV.setText(lesson.getContent().getSummary());
        }

        if (!basicLessonContent.getHomework().isEmpty()){
            homeworkTV.setText(lesson.getContent().getHomework());
        }

        setActivitiesToUI();

        setBehaviorToUI();
    }

    private void setBehaviorToUI() {
        TextView behaviorRatingTV    = (TextView) findViewById(R.id.lesson_viewer_behavior_rate);
        TextView behaviorNotes       = (TextView) findViewById(R.id.lesson_viewer_behavior_notes);
        assert behaviorRatingTV     != null;
        assert behaviorNotes        != null;

        if (lesson.getBehavior() != null){
            Behavior behavior = lesson.getBehavior();
            behaviorRatingTV.setText(behavior.getContent().getRating());
            behaviorNotes.setText(behavior.getContent().getNotes());
        }
    }

    private void setClassRoomToUI() {
        TextView studentNameTV        = (TextView) findViewById(R.id.lesson_viewer_student);
        TextView classRoomNameTV      = (TextView) findViewById(R.id.lesson_viewer_classroom);
        TextView schoolClassNameTV    = (TextView) findViewById(R.id.lesson_viewer_school_class);
        TextView schoolNameTV         = (TextView) findViewById(R.id.lesson_viewer_school);

        assert studentNameTV        != null;
        assert classRoomNameTV      != null;
        assert schoolClassNameTV    != null;
        assert schoolNameTV         != null;

        studentNameTV.setText(studentNameString);
        classRoomNameTV.setText(classroom.getName());
        schoolClassNameTV.setText(classroom.getSchool_class());
        schoolNameTV.setText(classroom.getSchool());
    }

    private void setUpToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        assert toolbar != null;
        toolbar.setTitle(lesson.getSubject() + " " + getString(R.string.lesson_keyword));
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(RESULT_CANCELED);
                finish();
                return true;

            case R.id.action_menu_discuss:
                Intent intent = new Intent(this, CommentsActivity.class);
                intent.putExtra(CommentsActivity.ID_ARG, lesson.getId());
                intent.putExtra(CommentsActivity.TYPE_ARG, Constant.LESSONS);
                intent.putExtra(CommentsActivity.TITLE_ARG,getString(R.string.lesson_keyword));
                startActivity(intent);
                break;
        }
        return false;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (lesson.getId() != 0){
            getMenuInflater().inflate(R.menu.discuss_menu, menu);
        }
        return true;
    }
}
