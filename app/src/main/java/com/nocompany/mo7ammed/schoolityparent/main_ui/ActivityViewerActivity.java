package com.nocompany.mo7ammed.schoolityparent.main_ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.nocompany.mo7ammed.schoolityparent.R;
import com.nocompany.mo7ammed.schoolityparent.model.Activity;
import com.nocompany.mo7ammed.schoolityparent.utilities.Constant;

public class ActivityViewerActivity extends AppCompatActivity {

    public static final String NOTIFICATION_SUMMARY = "notificationSummary";
    public static final String ACTIVITY_ARG = "activityObject";
    private Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewr_activity);

        activity = (Activity) getIntent().getSerializableExtra(ACTIVITY_ARG);

        setUpToolbar();

        setActivitiesToUI();

        setNotificationSummary();
    }

    private void setNotificationSummary() {
        CardView cardView = (CardView) findViewById(R.id.notification_summary_cardView);
        TextView textView = (TextView) findViewById(R.id.notification_summary);
        String notificationSummary = getIntent().getStringExtra(NOTIFICATION_SUMMARY);
        if (notificationSummary != null && !notificationSummary.isEmpty()){
            cardView.setVisibility(View.VISIBLE);
            textView.setText(notificationSummary);
        }
    }

    private void setActivitiesToUI() {

        TextView homeworkRatingTV       = (TextView) findViewById(R.id.lesson_viewer_homework_rate);
        TextView participationRatingTV  = (TextView) findViewById(R.id.lesson_viewer_participation_rate);
        TextView assessmentRatingTV     = (TextView) findViewById(R.id.lesson_viewer_assessment_rate);
        TextView notesTV                = (TextView) findViewById(R.id.lesson_viewer_activities_notes);

        assert homeworkRatingTV        != null;
        assert participationRatingTV   != null;
        assert assessmentRatingTV      != null;
        assert notesTV                 != null;

        homeworkRatingTV.setText(activity.getContent().getHomework());
        participationRatingTV.setText(activity.getContent().getParticipation());
        assessmentRatingTV.setText(activity.getContent().getAssessment());
        notesTV.setText(activity.getContent().getNotes());
    }

    private void setUpToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        assert toolbar != null;
        toolbar.setTitle("Student Activity");
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(RESULT_CANCELED);
                finish();
                return true;

            case R.id.action_menu_discuss:
                Intent intent = new Intent(this, CommentsActivity.class);
                intent.putExtra(CommentsActivity.ID_ARG, activity.getId());
                intent.putExtra(CommentsActivity.TYPE_ARG, Constant.ACTIVITIES);
                intent.putExtra(CommentsActivity.TITLE_ARG,getString(R.string.activity));
                startActivity(intent);
                break;
        }
        return false;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (activity.getId() != 0){
            getMenuInflater().inflate(R.menu.discuss_menu, menu);
        }
        return true;
    }
}
