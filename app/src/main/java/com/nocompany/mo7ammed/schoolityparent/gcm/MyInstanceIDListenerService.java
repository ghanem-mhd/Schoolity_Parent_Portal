package com.nocompany.mo7ammed.schoolityparent.gcm;

import android.content.Intent;

import com.google.android.gms.iid.InstanceIDListenerService;
import com.nocompany.mo7ammed.schoolityparent.utilities.PrefsKeys;
import com.pixplicity.easyprefs.library.Prefs;

public class MyInstanceIDListenerService extends InstanceIDListenerService {

    @Override
    public void onTokenRefresh() {
        Prefs.putBoolean(PrefsKeys.SENT_TOKEN_TO_SERVER,false);
        Intent intent = new Intent(this, RegistrationIntentService.class);
        startService(intent);
    }

}
