package com.nocompany.mo7ammed.schoolityparent.main_ui;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.nocompany.mo7ammed.schoolityparent.R;
import com.nocompany.mo7ammed.schoolityparent.api.FollowStudentRequest;
import com.nocompany.mo7ammed.schoolityparent.api.VolleyHandler;
import com.nocompany.mo7ammed.schoolityparent.model.Error;
import com.nocompany.mo7ammed.schoolityparent.model.FollowObject;
import com.nocompany.mo7ammed.schoolityparent.model.FollowedStudent;
import com.nocompany.mo7ammed.schoolityparent.model.JSONParser;
import com.nocompany.mo7ammed.schoolityparent.utilities.App;
import com.nocompany.mo7ammed.schoolityparent.utilities.BaseActivity;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FollowStudentActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.follow_student_code)
    EditText codeEditText;

    @BindView(R.id.follow_student_full_name)
    EditText fullNameEditText;

    @BindView(R.id.follow_student_other_relationship)
    EditText otherRelationshipEditText;

    @BindView(R.id.follow_student_relationship_spinner)
    Spinner relationshipSpinner;

    private boolean isOtherRelationship = false;
    private AdapterView.OnItemSelectedListener OnCatSpinnerCL = new AdapterView.OnItemSelectedListener() {
        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            ((TextView) parent.getChildAt(0)).setTextColor(ContextCompat.getColor(getApplicationContext(),R.color.gray));
            ((TextView) parent.getChildAt(0)).setTextSize(16);
            changeOtherRelationshipLayViability(pos);
        }

        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.follow_student_activity);
        ButterKnife.bind(this);
        relationshipSpinner.setOnItemSelectedListener(OnCatSpinnerCL);
        setUpToolbar();
    }

    private void setUpToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                    setResult(RESULT_CANCELED);
                    finish();
                return true;

            case R.id.action_menu_done:
                    onDoneClick();
                break;
        }
        return false;
    }

    private void onDoneClick() {
        FollowObject followObject = new FollowObject();

        followObject.setCode(codeEditText.getText().toString());
        followObject.setFull_name(fullNameEditText.getText().toString());
        if (isOtherRelationship){
            followObject.setRelationship(otherRelationshipEditText.getText().toString());
        }else{
            followObject.setRelationship(relationshipSpinner.getSelectedItem().toString());
        }

        int errorMessage = followObject.checkData();
        if (errorMessage == -1){
            API_Follow_Request(followObject);
        }else {
            showLongToast(getString(errorMessage));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.done_menu, menu);
        return true;
    }

    private void changeOtherRelationshipLayViability(int pos) {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.follow_student_other_relationship_layout);
        if (pos == 4){
            assert linearLayout != null;
            isOtherRelationship = true;
            linearLayout.setVisibility(View.VISIBLE);
        }else {
            assert linearLayout != null;
            isOtherRelationship = false;
            linearLayout.setVisibility(View.GONE);
        }
    }

    private void API_Follow_Request(FollowObject followObject) {
        App.hideSoftKeyboard(this);
        showLoadingDialog();

        FollowStudentRequest followStudentRequest = new FollowStudentRequest(this,followObject) {
            @Override
            protected void doOnResponse(JSONObject jsonObject) {
                API_Follow_Response(JSONParser.newFollowedStudent(jsonObject));
            }

            @Override
            protected void doOnError(VolleyError error) {
                VolleyHandler.handleVolleyError(error);
                API_Follow_Error(new Error(FollowStudentActivity.this.getApplicationContext(),error));
            }
        };

        followStudentRequest.request(false);
    }


    private void API_Follow_Response(FollowedStudent followedStudent){
        dismissLoadingDialog();
        showFinishDialog(followedStudent.getStudentName());
    }

    private void API_Follow_Error(Error error){
        dismissLoadingDialog();
        showSnackBar(error.getErrorMessage(),getString(android.R.string.ok), Snackbar.LENGTH_INDEFINITE,this);
    }


    private void showFinishDialog(String studentName) {
        final AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setIcon(R.drawable.ic_done_all_24dp)
                .setTitle(R.string.done)
                .setMessage(String.format(getString(R.string.successfully_followed_student),studentName))
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        setResult(RESULT_OK);
                        finish();
                    }
                })
                .create();

        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(getApplicationContext(),R.color.colorAccent));
            }
        });
        alertDialog.show();
    }

    @Override
    public void onClick(View v) {

    }
}
