package com.nocompany.mo7ammed.schoolityparent.utilities;

import com.google.gson.Gson;
import com.nocompany.mo7ammed.schoolityparent.model.UserInfo;
import com.pixplicity.easyprefs.library.Prefs;

public class PrefsHelper {

    public static void saveUserInfo(UserInfo userInfo) {
        Gson gson = new Gson();
        String userInfoString = gson.toJson(userInfo);
        Prefs.putString(PrefsKeys.USER_INFO, userInfoString);
    }

    public static UserInfo getUserInfo() {
        Gson gson = new Gson();
        return gson.fromJson(Prefs.getString(PrefsKeys.USER_INFO, null), UserInfo.class);
    }
}
