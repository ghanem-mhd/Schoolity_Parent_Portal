package com.nocompany.mo7ammed.schoolityparent.api;


import android.content.Context;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.nocompany.mo7ammed.schoolityparent.BuildConfig;

public abstract class DeleteCommentRequest extends Request {
    private static final String TAG = "DeleteCommentRequest";
    private static final int METHOD = Request.DELETE;
    private String URI = Request.REST_SERVER + "comments/%s";
    private Response.Listener<String> stringListener = new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {
            DeleteCommentRequest.this.doOnResponse(response);
        }
    };

    private Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            DeleteCommentRequest.this.doOnError(error);
        }
    };


    public DeleteCommentRequest(Context context, int commentID) {
        this.isAuthRequest = true;
        URI = String.format(URI, commentID);
        conHandler = new VolleyHandler(context);
    }


    @Override
    public void request(boolean withCaching) {
        CustomStringRequest customStringRequest = new CustomStringRequest(
                METHOD,
                URI,
                stringListener,
                errorListener,
                isAuthRequest);

        if (BuildConfig.DEBUG)
            Log.d(TAG, "making a request Method:" + METHOD + " URI:" + URI);

        conHandler.connect(customStringRequest);
    }

    protected abstract void doOnResponse(String response);
}
