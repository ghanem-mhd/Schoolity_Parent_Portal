package com.nocompany.mo7ammed.schoolityparent.utilities;


import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.nocompany.mo7ammed.schoolityparent.R;
import com.nocompany.mo7ammed.schoolityparent.model.Error;

public class BaseFragment extends Fragment {

    private ProgressDialog loadingDialog;
    private Snackbar snackbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadingDialog = getLoadingDialog();
    }

    public ProgressDialog getLoadingDialog() {
        ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage(getString(R.string.loading));
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        return dialog;
    }

    public void showLoadingDialog() {
        loadingDialog.show();
    }

    public void dismissLoadingDialog() {
        loadingDialog.dismiss();
    }

    public void showLongToast(String message){
        Toast.makeText(getActivity().getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    public void showShortToast(String message){
        Toast.makeText(getActivity().getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    public void debugLog(String logMessage){
        Log.d("test", logMessage);
    }

    public AlertDialog ShowConfirmDialog(String title, String message, String positive, String negative) {
        return new AlertDialog.Builder(getActivity())
                .setIcon(R.drawable.ic_warning_24dp)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(positive, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getActivity().finish();
                    }

                })
                .setNegativeButton(negative, null)
                .show();
    }

    public void ShowConfirmDialog(String message, String positive, String negative) {
        new AlertDialog.Builder(getActivity())
                .setIcon(R.drawable.ic_warning_24dp)
                .setMessage(message)
                .setPositiveButton(positive, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getActivity().finish();
                    }

                })
                .setNegativeButton(negative, null)
                .show();
    }

    public void showSnackBar(String message, int duration, String actionText, View.OnClickListener onClickListener) {
        snackbar = Snackbar.make(getView(), message, duration).setAction(actionText, onClickListener);
        snackbar.show();
    }


    public void On_API_Error(VolleyError volleyError, View.OnClickListener onClickListener) {
        if (!App.isNetworkAvailable(getActivity())) {
            showSnackBar(getString(R.string.offline_info_not_up_to_data), Snackbar.LENGTH_INDEFINITE, getString(R.string.retry), onClickListener);
        } else {
            Error error = new Error(getActivity(), volleyError);
            showLongToast(error.getErrorMessage());
        }
    }

    public void On_API_Error(VolleyError volleyError, View.OnClickListener onClickListener, TextView errorTextView) {
        if (!App.isNetworkAvailable(getActivity())) {
            errorTextView.setText(getString(R.string.no_connection));
            showSnackBar(getString(R.string.offline_info_not_up_to_data), Snackbar.LENGTH_INDEFINITE, getString(R.string.retry), onClickListener);
        } else {
            Error error = new Error(getActivity(), volleyError);
            errorTextView.setText(error.getErrorMessage());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (loadingDialog != null) {
            loadingDialog.dismiss();
        }

        if (snackbar != null) {
            snackbar.dismiss();
        }
    }
}
