package com.nocompany.mo7ammed.schoolityparent.gcm;


import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.android.volley.VolleyError;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.nocompany.mo7ammed.schoolityparent.R;
import com.nocompany.mo7ammed.schoolityparent.api.RegisterGCMToken;
import com.nocompany.mo7ammed.schoolityparent.utilities.PrefsKeys;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONObject;


public class RegistrationIntentService extends IntentService {
    private static final String TAG = "RegIntentService";

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            InstanceID instanceID = InstanceID.getInstance(this);
            String token = instanceID.getToken(getString(R.string.gcm_SenderId), GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);

            Log.i(TAG, "GCM Registration Token: " + token);

            if (!Prefs.getBoolean(PrefsKeys.SENT_TOKEN_TO_SERVER,false)){
                sendRegistrationToServer(token);
            }
        } catch (Exception e) {
            Log.d(TAG, "Failed to complete token refresh", e);
            Prefs.putBoolean(PrefsKeys.SENT_TOKEN_TO_SERVER,false);
        }

    }

    private void sendRegistrationToServer(String token) {
        RegisterGCMToken registerGCMToken = new RegisterGCMToken(this,token) {
            @Override
            protected void doOnResponse(JSONObject jsonObject) {
                Log.i(TAG, "GCM Token sent to API");
            }

            @Override
            protected void doOnError(VolleyError error) {
                Log.i(TAG, "Error in sending GCM token to API");
            }
        };
        registerGCMToken.request(false);
    }
}