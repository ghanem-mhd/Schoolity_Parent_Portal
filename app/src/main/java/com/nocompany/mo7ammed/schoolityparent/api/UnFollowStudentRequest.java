package com.nocompany.mo7ammed.schoolityparent.api;


import android.content.Context;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.nocompany.mo7ammed.schoolityparent.BuildConfig;

public abstract class UnFollowStudentRequest extends Request {
    private static final String TAG = "UnFollowStudentRequest";
    private static final int METHOD = Request.DELETE;
    private String URI = Request.REST_SERVER + "parent/followings/";
    private Response.Listener<String> stringListener = new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {
            UnFollowStudentRequest.this.doOnResponse(response);
        }
    };

    private Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            UnFollowStudentRequest.this.doOnError(error);
        }
    };


    public UnFollowStudentRequest(Context context, int followingID) {
        this.isAuthRequest = true;
        URI += followingID;
        conHandler = new VolleyHandler(context);
    }


    @Override
    public void request(boolean withCaching) {
        CustomStringRequest customStringRequest = new CustomStringRequest(
                METHOD,
                URI,
                stringListener,
                errorListener,
                true);

        if (BuildConfig.DEBUG)
            Log.d(TAG, "making a request Method:" + METHOD + " URI:" + URI);

        conHandler.connect(customStringRequest);
    }

    protected abstract void doOnResponse(String response);
}
