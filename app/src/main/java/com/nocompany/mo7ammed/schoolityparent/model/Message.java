package com.nocompany.mo7ammed.schoolityparent.model;


import android.content.Context;

import com.nocompany.mo7ammed.schoolityparent.R;

import java.io.Serializable;

public class Message implements Serializable{
    private int id;
    private String student;
    private String message_type;
    private String content_type;
    private Object content;
    private int comments_count;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStudent() {
        return student;
    }

    public void setStudent(String student) {
        this.student = student;
    }

    public String getMessage_type() {
        return message_type;
    }

    public void setMessage_type(String message_type) {
        this.message_type = message_type;
    }

    public int getComments_count() {
        return comments_count;
    }

    public void setComments_count(int comments_count) {
        this.comments_count = comments_count;
    }

    public String getContent_type() {
        return content_type;
    }

    public void setContent_type(String content_type) {
        this.content_type = content_type;
    }

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }

    public class AbsenceMessageContent implements Serializable{
        private String date;
        private String reason;

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getReason() {
            return reason;
        }

        public void setReason(String reason) {
            this.reason = reason;
        }
    }

    public class BasicMessageContent implements Serializable{
        private String subject;
        private String body;

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
        }
    }

    public String getType(Context context){
        if (message_type.equals("absence_justification")){
            return context.getString(R.string.absence_justification);
        }

        if (message_type.equals("complaint")){
            return context.getString(R.string.complaint);
        }

        if (message_type.equals("suggestion")){
            return context.getString(R.string.suggestion);
        }
        return "None";
    }

    public String getText(){
        if (content instanceof AbsenceMessageContent){
            AbsenceMessageContent absenceMessageContent = (AbsenceMessageContent) content;
            String text = "On %s because %s";
            return String.format(text,absenceMessageContent.getDate(),absenceMessageContent.getReason());
        }

        if (content instanceof BasicMessageContent){
            BasicMessageContent basicMessageContent = (BasicMessageContent) content;
            String text = "%s\n%s";
            return String.format(text,basicMessageContent.getSubject(),basicMessageContent.getBody());
        }

        return "None";
    }
}
