package com.nocompany.mo7ammed.schoolityparent.main_ui;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.nocompany.mo7ammed.schoolityparent.R;
import com.nocompany.mo7ammed.schoolityparent.api.AnnouncementsRequest;
import com.nocompany.mo7ammed.schoolityparent.api.FollowingStudentRequest;
import com.nocompany.mo7ammed.schoolityparent.api.VolleyHandler;
import com.nocompany.mo7ammed.schoolityparent.db_cache.JSONsDatabaseHelper;
import com.nocompany.mo7ammed.schoolityparent.helper.SimpleDividerItemDecoration;
import com.nocompany.mo7ammed.schoolityparent.model.Announcement;
import com.nocompany.mo7ammed.schoolityparent.model.JSON;
import com.nocompany.mo7ammed.schoolityparent.model.JSONParser;
import com.nocompany.mo7ammed.schoolityparent.ui_adapter.AnnouncementsAdapter;
import com.nocompany.mo7ammed.schoolityparent.utilities.BaseFragment;

import org.json.JSONArray;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AnnouncementsFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, AnnouncementsAdapter.AnnouncementsAdapterInterface, Toolbar.OnMenuItemClickListener, DialogInterface.OnClickListener {

    @BindView(R.id.announcements_recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.announcements_swipeRefresh)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.announcements_error_textView)
    TextView errorTextView;

    @BindView(R.id.announcements_error_scrollView)
    ScrollView scrollView;

    private AnnouncementsAdapter announcementsAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.announcements_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setUpToolBar();

        setUpAdsList();

        API_Grades_Request(true);
    }

    private void setUpToolBar() {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.main_toolbar);
        toolbar.setTitle(R.string.announcements);
        toolbar.inflateMenu(R.menu.announcements_menu);
        toolbar.setOnMenuItemClickListener(this);
    }

    private void setUpAdsList() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        announcementsAdapter = new AnnouncementsAdapter();

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(announcementsAdapter);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity().getApplicationContext()));

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getActivity(), R.color.colorAccent));

        announcementsAdapter.setAnnouncementsAdapterInterface(this);
    }

    private void API_Grades_Request(boolean fromCache) {
        if (!checkFollowing()){
            setRefreshing(false);
            showWarningTextView(getString(R.string.no_following_student_go));
            return;
        }
        setRefreshing(true);
        AnnouncementsRequest announcementsRequest = new AnnouncementsRequest(getActivity()) {
            @Override
            protected void doOnResponse(JSONArray jsonArray) {
                API_Grades_Response(jsonArray);
            }

            @Override
            protected void doOnError(VolleyError error) {
                VolleyHandler.handleVolleyError(error);
                API_Grades_Error(error);
            }
        };
        announcementsRequest.request(fromCache);
    }

    private void API_Grades_Response(JSONArray jsonArray) {
        setRefreshing(false);
        List<Announcement> announcements = JSONParser.getAnnouncementsList(jsonArray);
        if (announcements != null && announcements.size() > 0) {
            scrollView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);

            debugLog(announcements.toString());
            announcementsAdapter.setNewData(announcements);
            recyclerView.swapAdapter(announcementsAdapter, true);
        } else {
            showWarningTextView(getString(R.string.no_grades));
        }
    }

    private void API_Grades_Error(VolleyError volleyError) {
        setRefreshing(false);
        On_API_Error(volleyError, new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void showWarningTextView(String warningMessage) {
        recyclerView.setVisibility(View.GONE);
        scrollView.setVisibility(View.VISIBLE);
        errorTextView.setText(warningMessage);
    }


    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                API_Grades_Request(false);
            }
        }, 500);
    }

    private void setRefreshing(final boolean isRefreshing) {
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(isRefreshing);
            }
        });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.main_toolbar);
        toolbar.getMenu().clear();
    }

    @Override
    public void onAnnouncementClick(Announcement announcement, int position) {
        Intent intent = new Intent(getActivity(),AnnouncementViewerActivity.class);
        intent.putExtra(AnnouncementViewerActivity.ANNOUNCEMENT_OBJECT_ARG,announcement);
        startActivity(intent);
    }

    @Override
    public void onAnnouncementLongPress(View view, Announcement announcement, int position) {

    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        if (item.getItemId() == R.id.action_menu_filter) {
            showFilterDialog();
            return true;
        }
        return false;
    }

    private void showFilterDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.pick_type);
        builder.setItems(R.array.announcementsTypes, this);
        builder.show();
    }


    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case 0:
                announcementsAdapter.getFilter().filter("");
                break;
            case 1:
                announcementsAdapter.getFilter().filter(getString(R.string.student));
                break;
            case 2:
                announcementsAdapter.getFilter().filter(getString(R.string.classroom_without));
                break;
            case 3:
                announcementsAdapter.getFilter().filter(getString(R.string.school_class));
                break;
            case 4:
                announcementsAdapter.getFilter().filter(getString(R.string.school_without));
                break;
        }
    }

    private boolean checkFollowing() {
        JSONsDatabaseHelper jsoNsDatabaseHelper = JSONsDatabaseHelper.getInstance();
        JSON followedStudentJSON = jsoNsDatabaseHelper.getJSON(FollowingStudentRequest.URI);
        return followedStudentJSON != null && followedStudentJSON.getJsonArray().length() > 0;
    }

}
