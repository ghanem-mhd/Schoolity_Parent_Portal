package com.nocompany.mo7ammed.schoolityparent.model;


import java.io.Serializable;

public class NotificationObject implements Serializable{

    private String notifiable_type;
    private String notifiable_id;
    private String summary;
    private String role;
    private String notifiable;

    public String getNotifiable_type() {
        return notifiable_type;
    }

    public void setNotifiable_type(String notifiable_type) {
        this.notifiable_type = notifiable_type;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getNotifiable_id() {
        return notifiable_id;
    }

    public void setNotifiable_id(String notifiable_id) {
        this.notifiable_id = notifiable_id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getNotifiable() {
        return notifiable;
    }

    public void setNotifiable(String notifiable) {
        this.notifiable = notifiable;
    }

    @Override
    public String toString() {
        return "NotificationObject{" +
                "notifiable_type='" + notifiable_type + '\'' +
                ", notifiable_id=" + notifiable_id +
                ", summary='" + summary + '\'' +
                ", role='" + role + '\'' +
                ", notifiable='" + notifiable + '\'' +
                '}';
    }
}
