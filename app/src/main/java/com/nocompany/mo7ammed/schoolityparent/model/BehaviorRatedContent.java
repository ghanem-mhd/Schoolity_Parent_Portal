package com.nocompany.mo7ammed.schoolityparent.model;


import java.io.Serializable;

public class BehaviorRatedContent implements Serializable{
    private String rating;
    private String notes;


    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Override
    public String toString() {
        return "BehaviorRatedContent{" +
                "rating='" + rating + '\'' +
                ", notes='" + notes + '\'' +
                '}';
    }
}
