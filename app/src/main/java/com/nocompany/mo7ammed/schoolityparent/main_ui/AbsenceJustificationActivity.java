package com.nocompany.mo7ammed.schoolityparent.main_ui;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.nocompany.mo7ammed.schoolityparent.R;
import com.nocompany.mo7ammed.schoolityparent.api.AbsenceJustificationRequest;
import com.nocompany.mo7ammed.schoolityparent.api.FollowingStudentRequest;
import com.nocompany.mo7ammed.schoolityparent.api.VolleyHandler;
import com.nocompany.mo7ammed.schoolityparent.db_cache.JSONsDatabaseHelper;
import com.nocompany.mo7ammed.schoolityparent.model.AbsenceJustificationObject;
import com.nocompany.mo7ammed.schoolityparent.model.DateYMD;
import com.nocompany.mo7ammed.schoolityparent.model.Error;
import com.nocompany.mo7ammed.schoolityparent.model.FollowedStudent;
import com.nocompany.mo7ammed.schoolityparent.model.JSON;
import com.nocompany.mo7ammed.schoolityparent.model.JSONParser;
import com.nocompany.mo7ammed.schoolityparent.model.Value;
import com.nocompany.mo7ammed.schoolityparent.ui_adapter.SpinnerAdapter;
import com.nocompany.mo7ammed.schoolityparent.utilities.App;
import com.nocompany.mo7ammed.schoolityparent.utilities.BaseActivity;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AbsenceJustificationActivity extends BaseActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener, AdapterView.OnItemSelectedListener {

    private DateFormat dateFormat = new SimpleDateFormat("EEEE dd MMMM yyyy", Locale.ENGLISH);
    private DateYMD selectedDate = new DateYMD();
    private int followedStudentID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.absence_justification_activity);

        ButterKnife.bind(this);

        setUpToolbar();

        datePickerTextView.setOnClickListener(this);

        setUpStudentList();
    }

    private void setUpToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(RESULT_CANCELED);
                finish();
                return true;

            case R.id.action_menu_send:
                onSendClick();
                break;
        }
        return false;
    }

    private void onSendClick() {
        AbsenceJustificationObject object = new AbsenceJustificationObject();
        object.setDate(selectedDate);
        object.setReason(reasonEditText.getText().toString());

        int result = object.checkData();
        if (result == -1){
            API_Send_Request(object);
        }else {
            showLongToast(getString(result));
        }
    }

    private void API_Send_Request(AbsenceJustificationObject object) {
        showLoadingDialog();
        AbsenceJustificationRequest absenceJustificationRequest = new AbsenceJustificationRequest(
                this,
                followedStudentID,object) {
            @Override
            protected void doOnResponse(JSONObject jsonObject) {
                API_Send_Response(jsonObject);
            }

            @Override
            protected void doOnError(VolleyError error) {
                VolleyHandler.handleVolleyError(error);
                API_Send_Error(new Error(AbsenceJustificationActivity.this.getApplicationContext(),error));
            }
        };
        absenceJustificationRequest.request(false);
    }

    private void API_Send_Response(JSONObject jsonObject){
        dismissLoadingDialog();
        showLongToast(getString(R.string.your_message_has_been));
        setResult(RESULT_OK);
        finish();
    }

    private void API_Send_Error(Error error){
        dismissLoadingDialog();
        showSnackBar(error.getErrorMessage(),getString(android.R.string.ok), Snackbar.LENGTH_INDEFINITE,this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.absence_just_date_picker_textView:
                showDatePicker();
            break;
        }
    }

    private void showDatePicker() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.show(getFragmentManager(), "Datepickerdialog");
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();


        selectedDate.setYear(year);
        selectedDate.setMonth(monthOfYear);
        selectedDate.setDay(dayOfMonth);

        calendar.set(Calendar.YEAR,year);
        calendar.set(Calendar.MONTH,monthOfYear);
        calendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);
        datePickerTextView.setText(dateFormat.format(calendar.getTime()));
    }

    private void setUpStudentList() {
        JSONsDatabaseHelper jsoNsDatabaseHelper = JSONsDatabaseHelper.getInstance();
        JSON followedStudentJSON = jsoNsDatabaseHelper.getJSON(FollowingStudentRequest.URI);
        if (followedStudentJSON != null) {
            List<FollowedStudent> followedStudentList = JSONParser.getFollowedStudentsFromJsonArray(followedStudentJSON.getJsonArray());
            List<Value> values = new ArrayList<>();
            for (FollowedStudent followedStudent:followedStudentList){
                values.add(new Value(followedStudent.getFollowingID(),followedStudent.getStudentName()));
            }
            SpinnerAdapter spinnerAdapter = new SpinnerAdapter(this,values);
            studentSpinner.setAdapter(spinnerAdapter);
            studentSpinner.setOnItemSelectedListener(this);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        followedStudentID = (int) id;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.send_menu, menu);
        return true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        App.hideSoftKeyboard(this);
    }

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.absence_just_reason_editText)
    EditText reasonEditText;

    @BindView(R.id.absence_just_date_picker_textView)
    TextView datePickerTextView;

    @BindView(R.id.absence_just_studentSpinner)
    Spinner studentSpinner;
}
