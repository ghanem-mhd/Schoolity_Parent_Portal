package com.nocompany.mo7ammed.schoolityparent.ui_adapter;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nocompany.mo7ammed.schoolityparent.model.Value;

import java.util.ArrayList;
import java.util.List;

public class SpinnerAdapter extends BaseAdapter {

    private Activity activity;
    private List<Value> values;
    private String customColor;

    public SpinnerAdapter(Activity activity, List<Value> values) {
        this.activity   = activity;
        this.values     = values;
        customColor     = null;
    }

    public SpinnerAdapter(Activity activity, List<Value> values, String customColor, Value extraValue) {
        this.activity       = activity;
        this.values = new ArrayList<>();
        this.values.add(extraValue);
        this.values.addAll(values);
        this.customColor    = customColor;
    }

    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public Object getItem(int position) {
        return values.get(position);
    }

    @Override
    public long getItemId(int position) {
        return values.get(position).getId();
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder viewHolder;

        if (view == null) {
            LayoutInflater inflater = activity.getLayoutInflater();
            view = inflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.name = (TextView) view.findViewById(android.R.id.text1);

            if (customColor != null){
                viewHolder.name.setTextColor(Color.parseColor(customColor));
            }else {
                viewHolder.name.setTextColor(Color.parseColor("#525355"));
            }


            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        Value value = (Value) getItem(position);
        viewHolder.name.setText(value.getName());

        return view;
    }

    public void setCustomColor(String customColor){
        this.customColor = customColor;
    }

    public void addItem(Value value) {
        values.add(value);
    }

    static class ViewHolder {
        TextView name;
    }
}
