package com.nocompany.mo7ammed.schoolityparent.api;


import com.android.volley.VolleyError;

public abstract class Request {
    public static final String REST_SERVER  = "http://schoolity.herokuapp.com/api/v1/";
    public static final int GET             = com.android.volley.Request.Method.GET;
    public static final int POST            = com.android.volley.Request.Method.POST;
    public static final int DELETE          = com.android.volley.Request.Method.DELETE;

    protected VolleyHandler conHandler;

    protected Boolean isAuthRequest;

    public abstract void request(boolean withCaching);

    protected abstract void doOnError(VolleyError error);
}
