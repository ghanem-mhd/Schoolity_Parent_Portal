package com.nocompany.mo7ammed.schoolityparent.model;


import com.nocompany.mo7ammed.schoolityparent.R;

public class SignInObject {
    private String email;
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int checkData() {
        if (email == null || email.isEmpty()) {
            return R.string.check_email;
        }
        if (password == null || password.isEmpty()) {
            return R.string.check_password;
        }
        return -1;
    }

}
