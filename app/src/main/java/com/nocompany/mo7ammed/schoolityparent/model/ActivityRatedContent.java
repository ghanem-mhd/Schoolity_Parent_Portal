package com.nocompany.mo7ammed.schoolityparent.model;

import java.io.Serializable;

public class ActivityRatedContent implements Serializable{

    private String homework;
    private String participation;
    private String assessment;
    private ActivityOtherRated other;
    private String notes;


    public String getParticipation() {
        return participation;
    }

    public void setParticipation(String participation) {
        this.participation = participation;
    }

    public String getAssessment() {
        return assessment;
    }

    public void setAssessment(String assessment) {
        this.assessment = assessment;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public ActivityOtherRated getOther() {
        return other;
    }

    public void setOther(ActivityOtherRated other) {
        this.other = other;
    }

    public String getHomework() {
        return homework;
    }

    public void setHomework(String homework) {
        this.homework = homework;
    }

    @Override
    public String toString() {
        return "ActivityRatedContent{" +
                "homework='" + homework + '\'' +
                ", participation='" + participation + '\'' +
                ", assessment='" + assessment + '\'' +
                ", other=" + other +
                ", notes='" + notes + '\'' +
                '}';
    }
}
