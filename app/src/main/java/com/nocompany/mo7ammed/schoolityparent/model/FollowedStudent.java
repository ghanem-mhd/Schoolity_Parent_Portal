package com.nocompany.mo7ammed.schoolityparent.model;


import com.google.gson.Gson;

import java.io.Serializable;

public class FollowedStudent implements Serializable {
    private int id; //following_id
    private Student student;
    private String relationship;


    public int getFollowingID() {
        return id;
    }

    public void setFollowingID(int id) {
        this.id = id;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public String getStudentName() {
        return student.getName();
    }

    public int getStudentID() {
        return student.getId();
    }

    @Override
    public String toString() {
        return "FollowedStudent{" +
                "id=" + id +
                ", student=" + student +
                ", relationship='" + relationship + '\'' +
                '}';
    }

    public String toJsonObject() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
