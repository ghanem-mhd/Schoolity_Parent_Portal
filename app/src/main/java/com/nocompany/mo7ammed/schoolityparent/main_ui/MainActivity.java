package com.nocompany.mo7ammed.schoolityparent.main_ui;


import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.gson.Gson;
import com.nocompany.mo7ammed.schoolityparent.R;
import com.nocompany.mo7ammed.schoolityparent.gcm.RegistrationIntentService;
import com.nocompany.mo7ammed.schoolityparent.model.Absence;
import com.nocompany.mo7ammed.schoolityparent.model.Activity;
import com.nocompany.mo7ammed.schoolityparent.model.Announcement;
import com.nocompany.mo7ammed.schoolityparent.model.Behavior;
import com.nocompany.mo7ammed.schoolityparent.model.Grade;
import com.nocompany.mo7ammed.schoolityparent.model.Lesson;
import com.nocompany.mo7ammed.schoolityparent.model.Message;
import com.nocompany.mo7ammed.schoolityparent.model.NotificationObject;
import com.nocompany.mo7ammed.schoolityparent.model.UserInfo;
import com.nocompany.mo7ammed.schoolityparent.utilities.App;
import com.nocompany.mo7ammed.schoolityparent.utilities.BaseActivity;
import com.nocompany.mo7ammed.schoolityparent.utilities.Constant;
import com.nocompany.mo7ammed.schoolityparent.utilities.PrefsHelper;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    public static final String NOTIFICATION_ARG = "notificationObject";

    private int currentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        ButterKnife.bind(this);

        setActionBarDrawerToggle();

        new Handler().postDelayed(new Runnable() {public void run() {setFragment(R.id.drawer_item_daily_agenda);}}, 50);

        setUpUserInfo();

        NotificationObject notificationObject = (NotificationObject) getIntent().getSerializableExtra(NOTIFICATION_ARG);
        if (notificationObject != null){
            openNotificationObjectActivity(notificationObject,false);
        }

        if (checkPlayServices()) {
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        NotificationObject notificationObject = (NotificationObject) intent.getSerializableExtra(NOTIFICATION_ARG);
        if (notificationObject != null){
            openNotificationObjectActivity(notificationObject,true);
        }
    }

    private void openNotificationObjectActivity(NotificationObject notificationObject,Boolean wasOpen) {
        Gson gson = new Gson();
        String notifiableType = notificationObject.getNotifiable_type();

        if (notifiableType.equals("Announcement")){
            Announcement announcement = gson.fromJson(notificationObject.getNotifiable(),Announcement.class);
            if (announcement.getComments_count() == 0){
                Intent intent = new Intent(this,AnnouncementViewerActivity.class);
                intent.putExtra(AnnouncementViewerActivity.ANNOUNCEMENT_OBJECT_ARG,announcement);
                startActivity(intent);
            }else {
                Intent intent = new Intent(this,CommentsActivity.class);
                intent.putExtra(CommentsActivity.TITLE_ARG,getString(R.string.announcement));
                intent.putExtra(CommentsActivity.TYPE_ARG, Constant.ANNOUNCEMENTS);
                intent.putExtra(CommentsActivity.ID_ARG,announcement.getId());
                startActivity(intent);
            }
        }

        if (notifiableType.equals("Grade")){
            Grade grade = gson.fromJson(notificationObject.getNotifiable(),Grade.class);
            if (grade.getComments_count() == 0){
                Intent intent = new Intent(this,GradeViewerActivity.class);
                intent.putExtra(GradeViewerActivity.Grade_OBJECT_ARG,grade);
                startActivity(intent);
            }else {
                Intent intent = new Intent(this,CommentsActivity.class);
                intent.putExtra(CommentsActivity.TITLE_ARG,getString(R.string.grade));
                intent.putExtra(CommentsActivity.TYPE_ARG, Constant.GRADE);
                intent.putExtra(CommentsActivity.ID_ARG,grade.getId());
                startActivity(intent);
            }
        }

        if (notifiableType.equals("Absence")){
            Absence absence = gson.fromJson(notificationObject.getNotifiable(),Absence.class);
            if (absence.getComments_count() == 0){
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        setFragment(R.id.drawer_student_absences);
                    }
                }, 50);

            }else {
                Intent intent = new Intent(this,CommentsActivity.class);
                intent.putExtra(CommentsActivity.TITLE_ARG,getString(R.string.absence));
                intent.putExtra(CommentsActivity.TYPE_ARG, Constant.ABSENCES);
                intent.putExtra(CommentsActivity.ID_ARG,absence.getId());
                startActivity(intent);
            }
        }


        if (notifiableType.equals("Message")){
            Message message = gson.fromJson(notificationObject.getNotifiable(),Message.class);
            if (message.getComments_count() == 0){
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        setFragment(R.id.drawer_messages);
                    }
                }, 50);

            }else {
                Intent intent = new Intent(this,CommentsActivity.class);
                intent.putExtra(CommentsActivity.TITLE_ARG,getString(R.string.message));
                intent.putExtra(CommentsActivity.TYPE_ARG, Constant.MESSAGES);
                intent.putExtra(CommentsActivity.ID_ARG,message.getId());
                startActivity(intent);
            }
        }


        if (notifiableType.equals("Lesson")){
            Lesson lesson = gson.fromJson(notificationObject.getNotifiable(),Lesson.class);
            Intent intent = new Intent(this,CommentsActivity.class);
            intent.putExtra(CommentsActivity.TITLE_ARG,getString(R.string.lesson_keyword));
            intent.putExtra(CommentsActivity.TYPE_ARG, Constant.LESSONS);
            intent.putExtra(CommentsActivity.ID_ARG,lesson.getId());
            startActivity(intent);
        }


        if (notifiableType.equals("Activity")){
            Activity activity = gson.fromJson(notificationObject.getNotifiable(),Activity.class);
            if (activity.getComments_count() == 0){
                Intent intent = new Intent(this,ActivityViewerActivity.class);
                intent.putExtra(ActivityViewerActivity.NOTIFICATION_SUMMARY,notificationObject.getSummary());
                intent.putExtra(ActivityViewerActivity.ACTIVITY_ARG,activity);
                startActivity(intent);
            }else {
                Intent intent = new Intent(this,CommentsActivity.class);
                intent.putExtra(CommentsActivity.TITLE_ARG,getString(R.string.activity));
                intent.putExtra(CommentsActivity.TYPE_ARG, Constant.ACTIVITIES);
                intent.putExtra(CommentsActivity.ID_ARG,activity.getId());
                startActivity(intent);
            }
        }

        if (notifiableType.equals("Behavior")){
            Behavior behavior = gson.fromJson(notificationObject.getNotifiable(),Behavior.class);
            if (behavior.getComments_count() == 0){
                Intent intent = new Intent(this,BehaviorViewerActivity.class);
                intent.putExtra(BehaviorViewerActivity.NOTIFICATION_SUMMARY,notificationObject.getSummary());
                intent.putExtra(BehaviorViewerActivity.BEHAVIOR_ARG,behavior);
                startActivity(intent);
            }else {
                Intent intent = new Intent(this,CommentsActivity.class);
                intent.putExtra(CommentsActivity.TITLE_ARG,getString(R.string.behavior));
                intent.putExtra(CommentsActivity.TYPE_ARG, Constant.BEHAVIORS);
                intent.putExtra(CommentsActivity.ID_ARG,behavior.getId());
                startActivity(intent);
            }
        }


    }

    private void setUpUserInfo() {
        UserInfo userInfo = PrefsHelper.getUserInfo();
        String userName = "%s %s";


        View view = navigationView.getHeaderView(0);

        TextView userNameTextView = (TextView) view.findViewById(R.id.user_name_tv);
        TextView emailTextView    = (TextView) view.findViewById(R.id.user_email_tv);

        userNameTextView.setText(String.format(userName,userInfo.getFirst_name(),userInfo.getLast_name()));
        emailTextView.setText(userInfo.getEmail());
    }

    private void setActionBarDrawerToggle() {
        navigationView.setNavigationItemSelectedListener(this);
        setSupportActionBar(toolbar);

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                App.hideSoftKeyboard(MainActivity.this);
                super.onDrawerSlide(drawerView, slideOffset);
            }
        };

        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    @Override
    public boolean onNavigationItemSelected(final MenuItem item) {
        drawerLayout.closeDrawers();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setFragment(item.getItemId());
            }
        }, 350);
        return true;
    }


    private void setFragment(int fragmentIndex) {

        if (fragmentIndex != currentFragment) {
            currentFragment = fragmentIndex;
        } else {
            return;
        }

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();

        switch (fragmentIndex) {
            case R.id.drawer_item_daily_agenda:
                setTitle(R.string.daily_agenda);
                fragmentTransaction.replace(R.id.frame, new DailyAgendaFragment(), "DailyAgendaFragment");
                getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                break;

            case R.id.drawer_item_grades:
                getFragmentManager().popBackStack();
                fragmentTransaction.add(R.id.frame, new GradesFragment(), "GradesFragment");
                fragmentTransaction.addToBackStack(null);
                break;

            case R.id.drawer_student_absences:
                getFragmentManager().popBackStack();
                fragmentTransaction.add(R.id.frame, new AbsencesFragment(), "AbsencesFragment");
                fragmentTransaction.addToBackStack(null);
                break;


            case R.id.drawer_item_timetable:
                getFragmentManager().popBackStack();
                fragmentTransaction.add(R.id.frame, new TimetableFragment(), "TimetableFragment");
                fragmentTransaction.addToBackStack(null);
                break;

            case R.id.drawer_item_announcements:
                getFragmentManager().popBackStack();
                fragmentTransaction.add(R.id.frame, new AnnouncementsFragment(), "AnnouncementsFragment");
                fragmentTransaction.addToBackStack(null);
                break;

            case R.id.drawer_item_students_accounts:
                getFragmentManager().popBackStack();
                fragmentTransaction.add(R.id.frame, new StudentsAccountFragment(), "StudentsAccountFragment");
                fragmentTransaction.addToBackStack(null);
                break;

            case R.id.drawer_messages:
                getFragmentManager().popBackStack();
                fragmentTransaction.add(R.id.frame, new MessagesFragment(), "MessagesFragment");
                fragmentTransaction.addToBackStack(null);
                break;

            case R.id.drawer_item_news_feed:
                getFragmentManager().popBackStack();
                fragmentTransaction.add(R.id.frame, new FeedsFragment(), "FeedFragment");
                fragmentTransaction.addToBackStack(null);
                break;

            case R.id.drawer_item_settings:
                toolbar.setTitle(R.string.settings);
                getFragmentManager().popBackStack();
                fragmentTransaction.add(R.id.frame, new SettingsFragment(), "SettingsFragment");
                fragmentTransaction.addToBackStack(null);
                break;

        }
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            currentFragment = R.id.drawer_item_daily_agenda;
            changeToolBarTitleAndDrawer();
        }
    }

    private void changeToolBarTitleAndDrawer() {
        Fragment fragment = getFragmentManager().findFragmentById(R.id.frame);
        if (fragment instanceof DailyAgendaFragment){
            ((DailyAgendaFragment) fragment).studentMenuManager.refreshToolbar();
            navigationView.getMenu().getItem(0).setChecked(true);
        }
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, 9000).show();
            }
            return false;
        }
        return true;
    }

    @BindView(R.id.main_toolbar) Toolbar toolbar;
    @BindView(R.id.main_drawer) DrawerLayout drawerLayout;
    @BindView(R.id.main_navigation_view) NavigationView navigationView;
}
