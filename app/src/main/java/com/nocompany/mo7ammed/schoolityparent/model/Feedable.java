package com.nocompany.mo7ammed.schoolityparent.model;


import android.content.Context;

public interface Feedable {
    String getType(Context context);
    String getTitle(Context context);
    String getSummary(Context context);
}
