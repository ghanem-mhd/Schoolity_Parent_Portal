package com.nocompany.mo7ammed.schoolityparent.model;

import com.nocompany.mo7ammed.schoolityparent.R;

import java.io.Serializable;

public class AbsenceJustificationObject implements Serializable{

    private String type = "absence_justification";
    private DateYMD date;
    private String reason;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public DateYMD getDate() {
        return date;
    }

    public void setDate(DateYMD date) {
        this.date = date;
    }

    public int checkData() {
        if (reason == null || reason.isEmpty())
            return R.string.check_reason;
        if (date == null)
            return R.string.check_date;
        else {
                return date.checkData();
        }
    }
}
