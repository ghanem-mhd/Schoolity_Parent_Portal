package com.nocompany.mo7ammed.schoolityparent.main_ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.curioustechizen.ago.RelativeTimeTextView;
import com.nocompany.mo7ammed.schoolityparent.R;
import com.nocompany.mo7ammed.schoolityparent.model.Announcement;
import com.nocompany.mo7ammed.schoolityparent.utilities.App;
import com.nocompany.mo7ammed.schoolityparent.utilities.Constant;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.LinkedHashMap;

public class AnnouncementViewerActivity extends AppCompatActivity {

    public static final String ANNOUNCEMENT_OBJECT_ARG = "announcementObject";

    private Announcement announcement;

    private ImageLoader imageLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.announcement_viewer_activity);

        imageLoader     = ImageLoader.getInstance();
        announcement    = (Announcement) getIntent().getSerializableExtra(ANNOUNCEMENT_OBJECT_ARG);

        setUpToolbar();

        showAnnouncementInfo();
    }

    private void showAnnouncementInfo() {
        TextView title  = (TextView) findViewById(R.id.ann_viewer_title);
        TextView body   = (TextView) findViewById(R.id.ann_viewer_body);
        TextView type   = (TextView) findViewById(R.id.ann_viewer_type);
        ImageView image = (ImageView) findViewById(R.id.ann_viewer_imageView);
        RelativeTimeTextView createdAt = (RelativeTimeTextView) findViewById(R.id.ann_viewer_created_at);


        if (title != null && announcement.getTitle() != null && !announcement.getTitle().isEmpty()) {
            title.setText(announcement.getTitle());
        }

        if (image != null && announcement.getImage_url() != null && !announcement.getImage_url().isEmpty()){
            image.setVisibility(View.VISIBLE);
            imageLoader.displayImage("http://schoolity.herokuapp.com"+announcement.getImage_url(),image, App.ImageOptions);
        }

        if (body != null && announcement.getBody() != null && !announcement.getBody().isEmpty()) {
            body.setText(Html.fromHtml(announcement.getBody()));
        }

        if (type != null) {
            type.setText(String.format("%s.", announcement.getAnnounceable_type()));
        }

        if (createdAt != null) {
            createdAt.setReferenceTime(announcement.getCreateData().getTime());
        }



        String announcementType = announcement.getAnnounceable_type();
        if (announcementType.equals("Student")) {
            showStudentInfo(announcement.getAnnounceable());
        }

        if (announcementType.equals("SchoolClass")) {
            showSchoolClassInfo(announcement.getAnnounceable());
        }

        if (announcementType.equals("School")) {
            showSchoolInfo(announcement.getAnnounceable());
        }

        if (announcementType.equals("Classroom")) {
            showClassroomInfo(announcement.getAnnounceable());
        }

    }

    private void showClassroomInfo(Object classroom) {
        LinkedHashMap<String, String> classRoomMap = (LinkedHashMap<String, String>) classroom;
        CardView cardView = (CardView) findViewById(R.id.ann_viewer_classroom_card);
        TextView classroomTextView = (TextView) findViewById(R.id.ann_viewer_class_name);
        TextView schoolClass = (TextView) findViewById(R.id.ann_viewer_class_schoolClass);
        TextView school = (TextView) findViewById(R.id.ann_viewer_class_school);

        assert cardView != null;
        cardView.setVisibility(View.VISIBLE);
        assert classroomTextView != null;
        classroomTextView.setText(classRoomMap.get("name"));
        assert schoolClass != null;
        schoolClass.setText(classRoomMap.get("school_class"));
        assert school != null;
        school.setText(classRoomMap.get("school"));
    }

    private void showSchoolInfo(Object school) {
        LinkedHashMap<String, String> schoolMap = (LinkedHashMap<String, String>) school;
        CardView cardView = (CardView) findViewById(R.id.ann_viewer_school_card);
        TextView schoolTextView = (TextView) findViewById(R.id.ann_viewer_school);

        assert cardView != null;
        cardView.setVisibility(View.VISIBLE);
        assert schoolTextView != null;
        schoolTextView.setText(schoolMap.get("name"));
    }

    private void showSchoolClassInfo(Object schoolClass) {
        LinkedHashMap<String, String> schoolClassMap = (LinkedHashMap<String, String>) schoolClass;
        CardView cardView = (CardView) findViewById(R.id.ann_viewer_schoolClass_card);
        TextView schoolClassTextView = (TextView) findViewById(R.id.ann_viewer_schoolClass);
        TextView school = (TextView) findViewById(R.id.ann_viewer_schoolClass_school);

        assert cardView != null;
        cardView.setVisibility(View.VISIBLE);
        assert schoolClassTextView != null;
        schoolClassTextView.setText(schoolClassMap.get("name"));
        assert school != null;
        school.setText(schoolClassMap.get("school"));
    }

    private void showStudentInfo(Object student) {
        LinkedHashMap<String, String> studentMap = (LinkedHashMap<String, String>) student;
        CardView cardView = (CardView) findViewById(R.id.ann_viewer_student_card);
        TextView studentTextView = (TextView) findViewById(R.id.ann_viewer_student);

        assert cardView != null;
        cardView.setVisibility(View.VISIBLE);
        assert studentTextView != null;
        studentTextView.setText(studentMap.get("name"));
    }

    private void setUpToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        String announcements = getString(R.string.announcements);
        assert toolbar != null;
        toolbar.setTitle(String.format("%s " + announcements, announcement.getAnnounceable_type()));
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(RESULT_CANCELED);
                finish();
                return true;

            case R.id.action_menu_discuss:
                Intent intent = new Intent(this, CommentsActivity.class);
                intent.putExtra(CommentsActivity.ID_ARG, announcement.getId());
                intent.putExtra(CommentsActivity.TYPE_ARG, Constant.ANNOUNCEMENTS);
                intent.putExtra(CommentsActivity.TITLE_ARG,getString(R.string.announcement));
                startActivity(intent);
                break;
        }
        return false;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.discuss_menu, menu);
        return true;
    }
}
