package com.nocompany.mo7ammed.schoolityparent.api;

import android.content.Context;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.nocompany.mo7ammed.schoolityparent.BuildConfig;
import com.nocompany.mo7ammed.schoolityparent.db_cache.JSONsDatabaseHelper;
import com.nocompany.mo7ammed.schoolityparent.model.JSON;

import org.json.JSONArray;

public abstract class AbsencesRequest extends Request {
    private static final String TAG = "AbsencesRequest";
    private static final int METHOD = Request.GET;
    JSONsDatabaseHelper jsoNsDatabaseHelper;
    private String URI = Request.REST_SERVER + "parent/followings/%s/absences/";

    private Response.Listener<JSONArray> jsonArrayListener = new Response.Listener<JSONArray>() {
        @Override
        public void onResponse(JSONArray response) {
            jsoNsDatabaseHelper.addJSON(new JSON(URI, response.toString()));
            AbsencesRequest.this.doOnResponse(response);
        }
    };

    private Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            AbsencesRequest.this.doOnError(error);
        }
    };

    public AbsencesRequest(Context context, int followingID) {
        this.isAuthRequest = true;
        URI = String.format(URI, followingID);
        conHandler = new VolleyHandler(context);
        jsoNsDatabaseHelper = JSONsDatabaseHelper.getInstance();
    }

    @Override
    public void request(boolean withCaching) {

        if (withCaching) {
            checkCache();
        }

        CustomJsonArrayRequest customJsonObjectRequest = new CustomJsonArrayRequest(
                METHOD,
                URI,
                null,
                jsonArrayListener,
                errorListener,
                isAuthRequest);

        if (BuildConfig.DEBUG)
            Log.d(TAG, "making a request Method:" + METHOD + " URI:" + URI);

        conHandler.connect(customJsonObjectRequest);
    }

    private void checkCache() {
        JSON json = jsoNsDatabaseHelper.getJSON(URI);
        if (json != null) {
            AbsencesRequest.this.doOnResponse(json.getJsonArray());
        } else {
            if (BuildConfig.DEBUG)
                Log.d(TAG, " Absences Not Found in cache");
        }
    }

    protected abstract void doOnResponse(JSONArray jsonArray);

}
