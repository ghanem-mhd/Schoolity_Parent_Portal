package com.nocompany.mo7ammed.schoolityparent.main_ui;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.melnykov.fab.FloatingActionButton;
import com.nocompany.mo7ammed.schoolityparent.R;
import com.nocompany.mo7ammed.schoolityparent.api.MessagesRequest;
import com.nocompany.mo7ammed.schoolityparent.api.VolleyHandler;
import com.nocompany.mo7ammed.schoolityparent.helper.SimpleDividerItemDecoration;
import com.nocompany.mo7ammed.schoolityparent.model.FollowedStudent;
import com.nocompany.mo7ammed.schoolityparent.model.JSONParser;
import com.nocompany.mo7ammed.schoolityparent.model.Message;
import com.nocompany.mo7ammed.schoolityparent.ui_adapter.MessagesAdapter;
import com.nocompany.mo7ammed.schoolityparent.utilities.BaseFragment;
import com.nocompany.mo7ammed.schoolityparent.utilities.Constant;
import com.nocompany.mo7ammed.schoolityparent.utilities.StudentMenuManager;

import org.json.JSONArray;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MessagesFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener, MessagesAdapter.MessagesAdapterInterface, StudentMenuManager.StudentMenuInterface {

    private MessagesAdapter messagesAdapter;

    private StudentMenuManager studentMenuManager;
    private FollowedStudent currentSelectedStudent;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        studentMenuManager = new StudentMenuManager(this, getString(R.string.messages));
        studentMenuManager.setStudentMenuInterface(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.messages_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setUpAdsList();

        studentMenuManager.getFollowingStudent();

        floatingActionButton.setOnClickListener(this);
    }

    private void setUpAdsList() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        messagesAdapter = new MessagesAdapter(getActivity());

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(messagesAdapter);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity().getApplicationContext()));

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getActivity(), R.color.colorAccent));

        messagesAdapter.setMessagesAdapterInterface(this);

        floatingActionButton.attachToRecyclerView(recyclerView);
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                API_Messages_Request(false);
            }
        }, 500);
    }

    private void setRefreshing(final boolean isRefreshing) {
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(isRefreshing);
            }
        });
    }

    private void API_Messages_Request(boolean withCaching) {
        if (currentSelectedStudent == null){
            setRefreshing(false);
            return;
        }
        setRefreshing(!withCaching);
        MessagesRequest messagesRequest = new MessagesRequest(getActivity(),currentSelectedStudent.getFollowingID()) {
            @Override
            protected void doOnResponse(JSONArray jsonArray) {
                API_Messages_Response(jsonArray);
            }

            @Override
            protected void doOnError(VolleyError error) {
                VolleyHandler.handleVolleyError(error);
                API_Messages_Error(error);
            }
        };

        messagesRequest.request(withCaching);
    }

    private void API_Messages_Response(JSONArray jsonArray) {
        setRefreshing(false);
        List<Message> messages = JSONParser.getMessageList(jsonArray);
        if (messages != null && messages.size() > 0) {
            errorScrollView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            messagesAdapter.setNewData(messages);
        } else {
            errorScrollView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
    }

    private void API_Messages_Error(VolleyError volleyError) {
        setRefreshing(false);
        On_API_Error(volleyError, new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.messages_fab:
                Intent intent = new Intent(getActivity(), SendMessagesActivity.class);
                startActivityForResult(intent,0);
                break;
        }
    }

    @Override
    public void onMessageClick(Message message, int position) {
        Intent intent = new Intent(getActivity(),CommentsActivity.class);
        intent.putExtra(CommentsActivity.ID_ARG,message.getId());
        intent.putExtra(CommentsActivity.TYPE_ARG, Constant.MESSAGES);
        intent.putExtra(CommentsActivity.TITLE_ARG,getString(R.string.message));
        startActivity(intent);
    }

    @Override
    public void onChangeStudent(FollowedStudent followedStudent) {
        currentSelectedStudent = followedStudent;
        API_Messages_Request(true);
    }

    @Override
    public void onEmptyGetStudents() {
        floatingActionButton.setVisibility(View.GONE);
        setToolbarTitle();
        showWarningTextView(getString(R.string.no_following_student_go));
    }

    private void setToolbarTitle() {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.main_toolbar);
        toolbar.setTitle(getString(R.string.messages));
    }

    private void showWarningTextView(String warningMessage) {
        recyclerView.setVisibility(View.GONE);
        errorScrollView.setVisibility(View.VISIBLE);
        errorTextView.setText(warningMessage);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        studentMenuManager.clearMenuAndSave();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == android.app.Activity.RESULT_OK){
            API_Messages_Request(false);
        }
    }

    @BindView(R.id.messages_recyclerView) RecyclerView recyclerView;
    @BindView(R.id.messages_refreshLayout) SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.messages_error_textView) TextView errorTextView;
    @BindView(R.id.messages_error_scrollView) ScrollView errorScrollView;
    @BindView(R.id.messages_fab) FloatingActionButton floatingActionButton;
}
