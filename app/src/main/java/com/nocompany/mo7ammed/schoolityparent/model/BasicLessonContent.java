package com.nocompany.mo7ammed.schoolityparent.model;


import java.io.Serializable;

public class BasicLessonContent implements Serializable{

    private String title;
    private String summary;
    private String homework;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getHomework() {
        return homework;
    }

    public void setHomework(String homework) {
        this.homework = homework;
    }

    @Override
    public String toString() {
        return "BasicLessonContent{" +
                "title='" + title + '\'' +
                ", summary='" + summary + '\'' +
                ", homework='" + homework + '\'' +
                '}';
    }
}
