package com.nocompany.mo7ammed.schoolityparent.main_ui;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;

import com.android.volley.VolleyError;
import com.nocompany.mo7ammed.schoolityparent.R;
import com.nocompany.mo7ammed.schoolityparent.api.FollowingStudentRequest;
import com.nocompany.mo7ammed.schoolityparent.api.SendMessageRequest;
import com.nocompany.mo7ammed.schoolityparent.api.VolleyHandler;
import com.nocompany.mo7ammed.schoolityparent.db_cache.JSONsDatabaseHelper;
import com.nocompany.mo7ammed.schoolityparent.model.Error;
import com.nocompany.mo7ammed.schoolityparent.model.FollowedStudent;
import com.nocompany.mo7ammed.schoolityparent.model.JSON;
import com.nocompany.mo7ammed.schoolityparent.model.JSONParser;
import com.nocompany.mo7ammed.schoolityparent.model.MessageObject;
import com.nocompany.mo7ammed.schoolityparent.model.Value;
import com.nocompany.mo7ammed.schoolityparent.ui_adapter.SpinnerAdapter;
import com.nocompany.mo7ammed.schoolityparent.utilities.App;
import com.nocompany.mo7ammed.schoolityparent.utilities.BaseActivity;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ComplaintSuggestionSendActivity extends BaseActivity implements AdapterView.OnItemSelectedListener,View.OnClickListener{

    public static final String MESSAGE_TYPE_ARG  = "messageType";
    public static final String ACTIVITY_TITLE    = "activityTitle";

    private int followedStudentID = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.complaint_suggestion_send_activity);
        ButterKnife.bind(this);
        setUpToolbar();
        setUpStudentList();
    }

    private void setUpToolbar() {
        String activityTitle = getIntent().getStringExtra(ACTIVITY_TITLE);
        toolbar.setTitle(activityTitle);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(RESULT_CANCELED);
                finish();
                return true;

            case R.id.action_menu_send:
                onSendClick();
                break;
        }
        return false;
    }

    private void onSendClick() {
        String messageType = getIntent().getStringExtra(MESSAGE_TYPE_ARG);
        MessageObject messageObject = new MessageObject();

        messageObject.setBody(messageBodyEditText.getText().toString());
        messageObject.setSubject(subjectEditText.getText().toString());
        messageObject.setType(messageType);

        if (followedStudentID == -1){
            showLongToast(getString(R.string.check_student));
            return;
        }

        int errorMessage = messageObject.checkData();

        if (errorMessage == -1){
            API_Send_Request(messageObject);
        }else {
            showLongToast(getString(errorMessage));
        }
    }

    private void API_Send_Request(MessageObject messageObject) {
        App.hideSoftKeyboard(this);
        showLoadingDialog();
        SendMessageRequest sendMessageRequest = new SendMessageRequest(this,followedStudentID,messageObject) {
            @Override
            protected void doOnResponse(JSONObject jsonObject) {
                API_Send_Response(jsonObject);
            }

            @Override
            protected void doOnError(VolleyError error) {
                VolleyHandler.handleVolleyError(error);
                API_Send_Error(new Error(ComplaintSuggestionSendActivity.this.getApplicationContext(),error));
            }
        };
        sendMessageRequest.request(false);
    }

    private void API_Send_Response(JSONObject jsonObject){
        dismissLoadingDialog();
        showLongToast(getString(R.string.your_message_has_been));
        setResult(RESULT_OK);
        finish();
    }

    private void API_Send_Error(Error error){
        dismissLoadingDialog();
        showSnackBar(error.getErrorMessage(),getString(android.R.string.ok), Snackbar.LENGTH_INDEFINITE,this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.send_menu, menu);
        return true;
    }

    private void setUpStudentList() {
        JSONsDatabaseHelper jsoNsDatabaseHelper = JSONsDatabaseHelper.getInstance();
        JSON followedStudentJSON = jsoNsDatabaseHelper.getJSON(FollowingStudentRequest.URI);
        if (followedStudentJSON != null) {
            List<FollowedStudent> followedStudentList = JSONParser.getFollowedStudentsFromJsonArray(followedStudentJSON.getJsonArray());
            List<Value> values = new ArrayList<>();
            for (FollowedStudent followedStudent:followedStudentList){
                values.add(new Value(followedStudent.getFollowingID(),followedStudent.getStudentName()));
            }
            SpinnerAdapter spinnerAdapter = new SpinnerAdapter(this,values);
            studentSpinner.setAdapter(spinnerAdapter);
            studentSpinner.setOnItemSelectedListener(this);
        }
    }



    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.comp_sugg_subject)
    EditText subjectEditText;

    @BindView(R.id.comp_sugg_message)
    EditText messageBodyEditText;

    @BindView(R.id.comp_sugg_studentSpinner)
    Spinner studentSpinner;

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        followedStudentID = (int) id;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    protected void onStop() {
        super.onStop();
        App.hideSoftKeyboard(this);
    }
}
