package com.nocompany.mo7ammed.schoolityparent.reg_ui;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.android.volley.VolleyError;
import com.nocompany.mo7ammed.schoolityparent.R;
import com.nocompany.mo7ammed.schoolityparent.api.SignUpRequest;
import com.nocompany.mo7ammed.schoolityparent.main_ui.MainActivity;
import com.nocompany.mo7ammed.schoolityparent.model.Error;
import com.nocompany.mo7ammed.schoolityparent.model.JSONParser;
import com.nocompany.mo7ammed.schoolityparent.model.SignUpObject;
import com.nocompany.mo7ammed.schoolityparent.model.UserInfo;
import com.nocompany.mo7ammed.schoolityparent.utilities.App;
import com.nocompany.mo7ammed.schoolityparent.utilities.BaseActivity;
import com.nocompany.mo7ammed.schoolityparent.utilities.PrefsHelper;
import com.nocompany.mo7ammed.schoolityparent.utilities.PrefsKeys;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SignUpActivity extends BaseActivity {

    @BindView(R.id.signUp_email_et)
    EditText emailEditText;
    @BindView(R.id.signUp_password_et)
    EditText passwordEditText;
    @BindView(R.id.signUp_first_name_et)
    EditText firstNameEditText;
    @BindView(R.id.signUp_last_name_et)
    EditText lastNameEditText;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_activity);

        ButterKnife.bind(this);

        setUpToolbar();
    }

    private void setUpToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                ShowConfirmDialog(getString(R.string.close_warning), getString(android.R.string.ok), getString(android.R.string.no));
                return true;
        }
        return false;
    }

    public void submitClickListener(View view) {
        App.hideSoftKeyboard(this);
        SignUpObject signUpObject = new SignUpObject();

        signUpObject.setEmail(emailEditText.getText().toString());
        signUpObject.setPassword(passwordEditText.getText().toString());
        signUpObject.setFirst_name(firstNameEditText.getText().toString());
        signUpObject.setLast_name(lastNameEditText.getText().toString());

        int result = signUpObject.checkData();
        if (result == -1)
            API_SignUp_Request(signUpObject);
        else
            showLongToast(getString(result));
    }

    private void API_SignUp_Request(SignUpObject signUpObject) {
        showLoadingDialog();

        SignUpRequest signUpRequest = new SignUpRequest(getApplicationContext(), signUpObject) {
            @Override
            protected void doOnResponse(JSONObject jsonObject) {
                API_SignUp_Response(jsonObject);
            }

            @Override
            protected void doOnError(VolleyError error) {
                API_SignUp_Error(new Error(SignUpActivity.this.getApplicationContext(), error));
            }
        };
        signUpRequest.request(false);
    }

    private void API_SignUp_Response(JSONObject jsonObject) {
        dismissLoadingDialog();
        UserInfo userInfo = JSONParser.newUserInfo(jsonObject);
        finishSigningUp(userInfo);
    }

    private void API_SignUp_Error(Error error) {
        dismissLoadingDialog();
        showLongToast(error.getErrorMessage());
    }

    private void finishSigningUp(UserInfo userInfo) {
        debugLog(userInfo.toString());

        Prefs.putBoolean(PrefsKeys.IS_LOGIN, true);

        PrefsHelper.saveUserInfo(userInfo);

        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();

    }
}
