package com.nocompany.mo7ammed.schoolityparent.main_ui;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.nocompany.mo7ammed.schoolityparent.R;
import com.nocompany.mo7ammed.schoolityparent.api.AgendaRequest;
import com.nocompany.mo7ammed.schoolityparent.helper.SimpleDividerItemDecoration;
import com.nocompany.mo7ammed.schoolityparent.model.AgendaDay;
import com.nocompany.mo7ammed.schoolityparent.model.FollowedStudent;
import com.nocompany.mo7ammed.schoolityparent.model.JSONParser;
import com.nocompany.mo7ammed.schoolityparent.model.Lesson;
import com.nocompany.mo7ammed.schoolityparent.ui_adapter.AgendaAdapter;
import com.nocompany.mo7ammed.schoolityparent.utilities.BaseFragment;
import com.nocompany.mo7ammed.schoolityparent.utilities.StudentMenuManager;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.CalendarMode;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import org.json.JSONObject;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DailyAgendaFragment extends BaseFragment implements StudentMenuManager.StudentMenuInterface, OnDateSelectedListener, AgendaAdapter.AgendaAdapterInterface {

    private int currentYear;
    private int currentMonth;
    private int currentDay;
    private FollowedStudent currentFollowedStudent;

    private AgendaAdapter agendaAdapter;

    public StudentMenuManager studentMenuManager;

    public AgendaDay agendaDay;

    private Boolean agendaFound = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        studentMenuManager = new StudentMenuManager(this, getString(R.string.agenda));
        studentMenuManager.setStudentMenuInterface(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.daily_agenda_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setUpAdsList();

        setUpCalendar();

        studentMenuManager.getFollowingStudent();
    }

    private void setUpCalendar() {
        Calendar calendar   = Calendar.getInstance();
        currentYear         = calendar.get(Calendar.YEAR);
        currentMonth        = calendar.get(Calendar.MONTH);
        currentDay          = calendar.get(Calendar.DAY_OF_MONTH);

        materialCalendarView.setSelectedDate(calendar.getTime());
        materialCalendarView.setOnDateChangedListener(this);
    }

    private void setUpAdsList() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        agendaAdapter = new AgendaAdapter(getActivity());

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(agendaAdapter);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity().getApplicationContext()));

        agendaAdapter.setAgendaAdapterInterface(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        setUpCalendarViewMode();
    }

    private void setUpCalendarViewMode() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String modeID = sharedPref.getString(SettingsFragment.CALENDAR_VIEW, "0");
        if (modeID.equals("0")){
            materialCalendarView.state().edit().setCalendarDisplayMode(CalendarMode.MONTHS).commit();
        }else {
            materialCalendarView.state().edit().setCalendarDisplayMode(CalendarMode.WEEKS).commit();
        }
    }

    private void API_Agenda_Request() {
        if (currentFollowedStudent == null){
            showWarningTextView(getString(R.string.no_following_student_go));
            return;
        }
       agendaFound = false;
       showLoadingDialog();
       AgendaRequest agendaRequest = new AgendaRequest(getActivity(), currentFollowedStudent.getFollowingID(),currentYear, currentMonth,currentDay) {
            @Override
            protected void doOnResponse(JSONObject jsonObject) {
                API_Agenda_Response(jsonObject);
            }

            @Override
            protected void doOnError(VolleyError error) {
                API_Agenda_Error(error);
            }
        };
        agendaRequest.request(true);
    }


    private void API_Agenda_Response(JSONObject jsonObject){
        dismissLoadingDialog();

        agendaDay = JSONParser.getNewAgendaDay(jsonObject);
        if (agendaDay != null && agendaDay.getLessons() != null && agendaDay.getLessons().size() > 0){

            agendaFound = true; // found in cache

            if (!agendaDay.getContent().getSummary().isEmpty()){
                daySummaryTextView.setVisibility(View.VISIBLE);
                daySummaryTextView.setText(agendaDay.getContent().getSummary());
            }else {
                daySummaryTextView.setVisibility(View.GONE);
            }

            recyclerView.setVisibility(View.VISIBLE);
            errorTextView.setVisibility(View.GONE);
            agendaAdapter.setNewData(agendaDay.getLessons());
        }else {
            daySummaryTextView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.GONE);
            errorTextView.setVisibility(View.VISIBLE);
        }
    }

    private void API_Agenda_Error(VolleyError volleyError){
        dismissLoadingDialog();

        if (!agendaFound){
            daySummaryTextView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.GONE);
            errorTextView.setVisibility(View.VISIBLE);
            On_API_Error(volleyError, new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            },errorTextView);
        }else {
            On_API_Error(volleyError, new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }
    }

    @Override
    public void onChangeStudent(FollowedStudent followedStudent) {
        currentFollowedStudent = followedStudent;
        API_Agenda_Request();
    }

    @Override
    public void onEmptyGetStudents() {
        showWarningTextView(getString(R.string.no_following_student_go));
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        studentMenuManager.clearMenuAndSave();
    }



    @Override
    public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
        currentYear     = date.getYear();
        currentMonth    = date.getMonth()+1;
        currentDay      = date.getDay();
        API_Agenda_Request();
    }

    @Override
    public void onLessonClick(Lesson lesson, int position) {
        Intent intent = new Intent(getActivity(),LessonViewerActivity.class);
        intent.putExtra(LessonViewerActivity.LESSON_OBJECT_ARG,lesson);
        intent.putExtra(LessonViewerActivity.STUDENT_NAME_ARG,currentFollowedStudent.getStudentName());
        intent.putExtra(LessonViewerActivity.CLASSROOM_OBJECT_ARG,agendaDay.getClassroom());
        startActivity(intent);
    }

    private void showWarningTextView(String warningMessage) {
        recyclerView.setVisibility(View.GONE);
        errorTextView.setVisibility(View.VISIBLE);
        errorTextView.setText(warningMessage);
    }

    @BindView(R.id.agenda_calendarView) MaterialCalendarView materialCalendarView;

    @BindView(R.id.agenda_recyclerView) RecyclerView recyclerView;

    @BindView(R.id.agenda_error_tv) TextView errorTextView;

    @BindView(R.id.agenda_day_summary) TextView daySummaryTextView;

}
