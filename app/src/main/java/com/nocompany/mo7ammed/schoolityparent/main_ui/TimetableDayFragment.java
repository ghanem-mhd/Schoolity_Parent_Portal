package com.nocompany.mo7ammed.schoolityparent.main_ui;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nocompany.mo7ammed.schoolityparent.R;
import com.nocompany.mo7ammed.schoolityparent.helper.SimpleDividerItemDecoration;
import com.nocompany.mo7ammed.schoolityparent.model.TimetableDay;
import com.nocompany.mo7ammed.schoolityparent.ui_adapter.TimeDayAdapter;
import com.nocompany.mo7ammed.schoolityparent.utilities.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TimetableDayFragment extends BaseFragment {

    public static final String ARG_PERIOD = "timetableDayObject";

    @BindView(R.id.timetable_day_recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.timetable_day_empty_tv)
    TextView emptyTextView;

    private TimetableDay timetableDay;

    public static TimetableDayFragment newInstance(TimetableDay timetableDay) {
        TimetableDayFragment fragment = new TimetableDayFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(ARG_PERIOD, timetableDay);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        timetableDay = getArguments().getParcelable(ARG_PERIOD);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.timetable_day_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUpAdsList();
    }

    private void setUpAdsList() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity().getApplicationContext()));

        if (timetableDay.getPeriods() != null && timetableDay.getPeriods().size() > 0) {
            TimeDayAdapter timeDayAdapter = new TimeDayAdapter(timetableDay.getPeriods());
            recyclerView.setAdapter(timeDayAdapter);
        } else {
            emptyTextView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
    }
}
