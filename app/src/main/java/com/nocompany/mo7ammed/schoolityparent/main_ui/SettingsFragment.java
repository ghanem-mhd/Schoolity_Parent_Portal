package com.nocompany.mo7ammed.schoolityparent.main_ui;


import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.nocompany.mo7ammed.schoolityparent.R;
import com.nocompany.mo7ammed.schoolityparent.api.FollowingStudentRequest;
import com.nocompany.mo7ammed.schoolityparent.api.SignOutReq;
import com.nocompany.mo7ammed.schoolityparent.db_cache.JSONsDatabaseHelper;
import com.nocompany.mo7ammed.schoolityparent.model.Error;
import com.nocompany.mo7ammed.schoolityparent.model.FollowedStudent;
import com.nocompany.mo7ammed.schoolityparent.model.JSON;
import com.nocompany.mo7ammed.schoolityparent.model.JSONParser;
import com.nocompany.mo7ammed.schoolityparent.reg_ui.SignInActivity;
import com.nocompany.mo7ammed.schoolityparent.utilities.PrefsKeys;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.List;

public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener, Preference.OnPreferenceClickListener {

    public static final String STUDENT          = "setting_default_student";
    public static final String LANGUAGE         = "setting_language";
    public static final String CALENDAR_VIEW    = "setting_calendar_view";
    public static final String IS_LOGIN         = "setting_logout";

    private ProgressDialog loadingDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.settings);

        findPreference(IS_LOGIN).setOnPreferenceClickListener(this);

        setUpStudentList();

        loadingDialog = getLoadingDialog();
    }

    private void setUpStudentList() {
        ListPreference studentsListPreference   = (ListPreference) findPreference(STUDENT);
        JSONsDatabaseHelper jsoNsDatabaseHelper = JSONsDatabaseHelper.getInstance();
        JSON followedStudentJSON = jsoNsDatabaseHelper.getJSON(FollowingStudentRequest.URI);
        if (followedStudentJSON != null) {
            List<FollowedStudent> followedStudentList = JSONParser.getFollowedStudentsFromJsonArray(followedStudentJSON.getJsonArray());
            String [] entries           = new String[followedStudentList.size()];
            String [] entriesValues     = new String[followedStudentList.size()];

            for (int i = 0 ; i < followedStudentList.size() ; i++){
                FollowedStudent followedStudent = followedStudentList.get(i);
                entries[i]           = followedStudent.getStudentName();
                entriesValues[i]     = String.valueOf(followedStudent.getFollowingID());
            }

            studentsListPreference.setEntries(entries);
            studentsListPreference.setEntryValues(entriesValues);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        assert view != null;
        view.setBackgroundColor(ContextCompat.getColor(getActivity(),R.color.white_background));
        return view;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(LANGUAGE)) {
            changeApplicationLanguage();
        }
    }

    private void logOut() {
        loadingDialog.show();
        SignOutReq signOutReq = new SignOutReq(getActivity()) {
            @Override
            protected void doOnResponse(String response) {
                loadingDialog.dismiss();
                Prefs.putBoolean(PrefsKeys.IS_LOGIN, false);
                JSONsDatabaseHelper.getInstance(getActivity()).clearCache();
                Intent intent = new Intent(getActivity(), SignInActivity.class);
                startActivity(intent);
                getActivity().finish();
            }

            @Override
            protected void doOnError(VolleyError error) {
                loadingDialog.dismiss();
                Error error1 = new Error(getActivity(),error);
                Toast.makeText(getActivity(),error1.getErrorMessage(),Toast.LENGTH_LONG).show();
            }
        };
        signOutReq.request(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }


    private void changeApplicationLanguage() {
        Intent intent = getActivity().getBaseContext().getPackageManager().getLaunchIntentForPackage(getActivity().getBaseContext().getPackageName());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        if (preference.getKey().equals(IS_LOGIN)) {
            logOut();
            return true;
        }
        return false;
    }

    public ProgressDialog getLoadingDialog(){
        ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage(getString(R.string.loading));
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        return dialog;
    }

}
