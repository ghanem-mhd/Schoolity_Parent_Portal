package com.nocompany.mo7ammed.schoolityparent.utilities;


import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.VolleyError;
import com.nocompany.mo7ammed.schoolityparent.R;
import com.nocompany.mo7ammed.schoolityparent.api.FollowingStudentRequest;
import com.nocompany.mo7ammed.schoolityparent.db_cache.JSONsDatabaseHelper;
import com.nocompany.mo7ammed.schoolityparent.main_ui.SettingsFragment;
import com.nocompany.mo7ammed.schoolityparent.model.FollowedStudent;
import com.nocompany.mo7ammed.schoolityparent.model.JSON;
import com.nocompany.mo7ammed.schoolityparent.model.JSONParser;

import org.json.JSONArray;

import java.util.List;

public class StudentMenuManager implements Toolbar.OnMenuItemClickListener {

    private BaseFragment fragment;
    private String toolbarTitle;
    private Toolbar toolbar;
    private StudentMenuInterface studentMenuInterface;
    private int defaultStudentID = -1;
    private String fragmentTitle;
    List<FollowedStudent> followedStudents;

    public StudentMenuManager(BaseFragment baseFragment, String fragmentTitle) {
        this.fragment       = baseFragment;
        this.fragmentTitle  = fragmentTitle;
        toolbar             = (Toolbar) fragment.getActivity().findViewById(R.id.main_toolbar);
        toolbar.setOnMenuItemClickListener(this);
        getDefaultStudentID();
    }

    private void getDefaultStudentID() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(fragment.getActivity());
        defaultStudentID = Integer.parseInt(sharedPref.getString(SettingsFragment.STUDENT,"-1"));
    }

    public void setStudentMenuInterface(StudentMenuInterface studentMenuInterface) {
        this.studentMenuInterface = studentMenuInterface;
    }

    public void getFollowingStudent() {
        List<FollowedStudent> cachedFollowedStudent = getFromCache();
        if (cachedFollowedStudent != null) {  // we have followed student list in cache
            setUpFollowedStudentMenu(cachedFollowedStudent);
        } else { // cache is empty make request
            getFromAPI();
        }
    }

    private List<FollowedStudent> getFromCache() {
        JSONsDatabaseHelper jsoNsDatabaseHelper = JSONsDatabaseHelper.getInstance();
        JSON followedStudentJSON = jsoNsDatabaseHelper.getJSON(FollowingStudentRequest.URI);
        if (followedStudentJSON != null) { // we have followed student list in cache
            return JSONParser.getFollowedStudentsFromJsonArray(followedStudentJSON.getJsonArray());
        }
        return null;
    }

    private void getFromAPI() {
        fragment.showLoadingDialog();
        FollowingStudentRequest followingStudentRequest = new FollowingStudentRequest(fragment.getActivity()) {
            @Override
            protected void doOnResponse(JSONArray jsonArray) {
                API_Response(jsonArray);
            }

            @Override
            protected void doOnError(VolleyError error) {
                API_Error(error);
            }
        };
        followingStudentRequest.request(false);
    }

    private void API_Response(JSONArray jsonArray) {
        fragment.dismissLoadingDialog();
        setUpFollowedStudentMenu(JSONParser.getFollowedStudentsFromJsonArray(jsonArray));
    }

    private void API_Error(VolleyError volleyError) {
        fragment.dismissLoadingDialog();
        fragment.On_API_Error(volleyError, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFollowingStudent();
            }
        });
    }

    private void setUpFollowedStudentMenu(List<FollowedStudent> followedStudents) {
        if (followedStudents != null) {
            if (followedStudents.size() == 0) {
                studentMenuInterface.onEmptyGetStudents();
            }
            if (followedStudents.size() > 0) {
                this.followedStudents = followedStudents;
                inflateFollowingStudentMenu();
                if (defaultStudentID == -1) {
                    onStudentSelected(followedStudents.get(0));
                } else
                {
                    for (FollowedStudent followedStudent:followedStudents){
                        if (followedStudent.getFollowingID() == defaultStudentID){
                            onStudentSelected(followedStudent);
                            break;
                        }
                    }
                }
            }
        }
    }


    private void inflateFollowingStudentMenu() {
        toolbar.getMenu().clear();
        if (followedStudents == null)
            return;
        for (int i = 0; i < followedStudents.size(); i++) {
            FollowedStudent followedStudent = followedStudents.get(i);
            Intent intent = new Intent();
            intent.putExtra("FollowedStudent", followedStudent);
            toolbar.getMenu().add(0, followedStudent.getFollowingID(), i, followedStudent.getStudentName()).setIntent(intent);
        }
    }


    @Override
    public boolean onMenuItemClick(MenuItem item) {
        FollowedStudent selectedStudent = (FollowedStudent) item.getIntent().getSerializableExtra("FollowedStudent");
        onStudentSelected(selectedStudent);
        return true;
    }

    private void onStudentSelected(FollowedStudent selectedStudent) {
        studentMenuInterface.onChangeStudent(selectedStudent);
        updateToolbarTitle(selectedStudent);
    }

    private void updateToolbarTitle(FollowedStudent followedStudent) {
        toolbarTitle = followedStudent.getStudentName() + " " + fragmentTitle;
        toolbar.setTitle(toolbarTitle);
    }

    public void clearMenuAndSave() {
        toolbar.getMenu().clear();
    }

    public interface StudentMenuInterface {
        void onChangeStudent(FollowedStudent followedStudent);

        void onEmptyGetStudents();
    }

    public void refreshToolbar(){
        inflateFollowingStudentMenu();
        if (toolbarTitle == null || toolbarTitle.isEmpty()){
            toolbar.setTitle(fragment.getString(R.string.daily_agenda));
        }else {
            toolbar.setTitle(toolbarTitle);
        }
    }
}
