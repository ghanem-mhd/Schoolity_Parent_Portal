package com.nocompany.mo7ammed.schoolityparent.model;


import java.io.Serializable;

public class Subject implements Serializable {
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Subject{" +
                "name='" + name + '\'' +
                '}';
    }
}