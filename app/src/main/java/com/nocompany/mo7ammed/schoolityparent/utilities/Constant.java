package com.nocompany.mo7ammed.schoolityparent.utilities;


public class Constant {
    public static final String ENGLISH              = "0";
    public static final String ARABIC               = "1";
    public static final String GRADE                = "grades";
    public static final String ANNOUNCEMENTS        = "announcements";
    public static final String LESSONS              = "lessons";
    public static final String ABSENCES             = "absences";
    public static final String COMPLAINT            = "complaint";
    public static final String SUGGESTION           = "suggestion";
    public static final String ABSENCES_JUST        = "absence_justification";
    public static final String MESSAGES             = "messages";
    public static final String ACTIVITIES           =  "activities";
    public static final String BEHAVIORS            =  "behaviors";
}
