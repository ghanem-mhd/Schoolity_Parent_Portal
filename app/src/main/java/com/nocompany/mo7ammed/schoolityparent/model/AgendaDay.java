package com.nocompany.mo7ammed.schoolityparent.model;


import java.io.Serializable;
import java.util.List;

public class AgendaDay implements Serializable{

    private String date;
    private String content_type;
    private BasicDayContent content;
    private Behavior behavior;
    private Classroom classroom;
    private List<Lesson> lessons;


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Classroom getClassroom() {
        return classroom;
    }

    public void setClassroom(Classroom classroom) {
        this.classroom = classroom;
    }

    public List<Lesson> getLessons() {
        return lessons;
    }

    public void setLessons(List<Lesson> lessons) {
        this.lessons = lessons;
    }

    public Behavior getBehavior() {
        return behavior;
    }

    public void setBehavior(Behavior behavior) {
        this.behavior = behavior;
    }

    public BasicDayContent getContent() {
        return content;
    }

    public void setContent(BasicDayContent content) {
        this.content = content;
    }

    public String getContent_type() {
        return content_type;
    }

    public void setContent_type(String content_type) {
        this.content_type = content_type;
    }

    @Override
    public String toString() {
        return "AgendaDay{" +
                "date='" + date + '\'' +
                ", content_type='" + content_type + '\'' +
                ", content=" + content +
                ", behavior=" + behavior +
                ", classroom=" + classroom +
                ", lessons=" + lessons +
                '}';
    }
}
