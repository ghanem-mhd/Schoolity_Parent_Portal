package com.nocompany.mo7ammed.schoolityparent.utilities;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.nocompany.mo7ammed.schoolityparent.R;
import com.nocompany.mo7ammed.schoolityparent.model.Error;

public class BaseActivity extends AppCompatActivity {

    private Snackbar snackbar;
    private ProgressDialog loadingDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadingDialog = getLoadingDialog();
    }


    public void showLongToast(String message){
        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
    }

    public void showShortToast(String message){
        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
    }


    public void debugLog(String logMessage){
        Log.v("test", logMessage);
    }

    public ProgressDialog getLoadingDialog(){
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.loading));
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        return dialog;
    }

    public void showLoadingDialog(){
        loadingDialog.show();
    }

    public void dismissLoadingDialog(){
        loadingDialog.dismiss();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (snackbar != null){
            snackbar.dismiss();
        }
    }


    public AlertDialog ShowConfirmDialog(String title, String message, String positive, String negative) {
        return new AlertDialog.Builder(this)
                .setIcon(R.drawable.ic_warning_24dp)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(positive, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                })
                .setNegativeButton(negative, null)
                .show();
    }

    public void ShowConfirmDialog(String message, String positive, String negative) {
        new AlertDialog.Builder(this)
                .setIcon(R.drawable.ic_warning_24dp)
                .setMessage(message)
                .setPositiveButton(positive, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                })
                .setNegativeButton(negative, null)
                .show();
    }

    public void showSnackBar(String message, String actionText, int length, View.OnClickListener onClickListener) {
        Snackbar.make(findViewById(android.R.id.content), message, length).setAction(actionText, onClickListener).show();
    }

    public void showSnackBar(String message, int duration, String actionText, View.OnClickListener onClickListener) {
        snackbar = Snackbar.make(findViewById(android.R.id.content), message, duration).setAction(actionText, onClickListener);
        snackbar.show();
    }


    public void On_API_Error(VolleyError volleyError, View.OnClickListener onClickListener) {
        if (!App.isNetworkAvailable(this)) {
            showSnackBar(getString(R.string.offline_info_not_up_to_data), Snackbar.LENGTH_INDEFINITE, getString(R.string.retry), onClickListener);
        } else {
            Error error = new Error(this, volleyError);
            showLongToast(error.getErrorMessage());
        }
    }

    public void On_API_Error(VolleyError volleyError, View.OnClickListener onClickListener, TextView errorTextView) {
        if (!App.isNetworkAvailable(this)) {
            showSnackBar(getString(R.string.offline_info_not_up_to_data), Snackbar.LENGTH_INDEFINITE, getString(R.string.retry), onClickListener);
        } else {
            Error error = new Error(this, volleyError);
            errorTextView.setText(error.getErrorMessage());
        }
    }
}
