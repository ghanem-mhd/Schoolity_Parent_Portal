package com.nocompany.mo7ammed.schoolityparent.model;


import java.io.Serializable;

public class ActivityOtherRated implements Serializable{
    private String name;
    private String rating;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "ActivityOtherRated{" +
                "name='" + name + '\'' +
                ", rating='" + rating + '\'' +
                '}';
    }
}
