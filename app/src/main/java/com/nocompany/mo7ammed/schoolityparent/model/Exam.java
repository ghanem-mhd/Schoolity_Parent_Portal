package com.nocompany.mo7ammed.schoolityparent.model;


import java.io.Serializable;

public class Exam implements Serializable {
    private int id;
    private String date;
    private String score;
    private String minimum_score;
    private Classroom classroom;
    private Subject subject;
    private String description;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMinimum_score() {
        return minimum_score;
    }

    public void setMinimum_score(String minimum_score) {
        this.minimum_score = minimum_score;
    }

    public Classroom getClassroom() {
        return classroom;
    }

    public void setClassroom(Classroom classroom) {
        this.classroom = classroom;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Exam{" +
                "id=" + id +
                ", date='" + date + '\'' +
                ", score='" + score + '\'' +
                ", minimum_score='" + minimum_score + '\'' +
                ", classroom=" + classroom +
                ", subject=" + subject +
                ", description='" + description + '\'' +
                '}';
    }
}