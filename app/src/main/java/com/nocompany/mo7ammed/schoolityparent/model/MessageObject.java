package com.nocompany.mo7ammed.schoolityparent.model;


import com.nocompany.mo7ammed.schoolityparent.R;

public class MessageObject {

    private String subject;
    private String body;
    private String type;

    public MessageObject() {
    }

    public MessageObject(String subject, String body, String type) {
        this.subject = subject;
        this.body = body;
        this.type = type;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int checkData() {
        if (subject == null || subject.isEmpty()) {
            return R.string.check_subject;
        }
        if (body == null || body.isEmpty()) {
            return R.string.check_message;
        }
        return -1;
    }
}
