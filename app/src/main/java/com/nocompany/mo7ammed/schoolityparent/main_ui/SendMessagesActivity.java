package com.nocompany.mo7ammed.schoolityparent.main_ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.nocompany.mo7ammed.schoolityparent.R;
import com.nocompany.mo7ammed.schoolityparent.utilities.BaseActivity;
import com.nocompany.mo7ammed.schoolityparent.utilities.Constant;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SendMessagesActivity extends BaseActivity implements View.OnClickListener {

    private static final int SEND_MESSAGE_REQ = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.send_messages_activity);

        ButterKnife.bind(this);

        setUpToolbar();

        sendComplaintRelativeLayout.setOnClickListener(this);
        sendSuggestionRelativeLayout.setOnClickListener(this);
        absenceJustificationRelativeLayout.setOnClickListener(this);
    }

    private void setUpToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(RESULT_CANCELED);
                finish();
                return true;
        }
        return false;
    }


    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()){
            case R.id.send_messages_send_complaint:
                intent = new Intent(this,ComplaintSuggestionSendActivity.class);
                intent.putExtra(ComplaintSuggestionSendActivity.MESSAGE_TYPE_ARG, Constant.COMPLAINT);
                intent.putExtra(ComplaintSuggestionSendActivity.ACTIVITY_TITLE,getString(R.string.send_complaint));
                break;

            case R.id.send_messages_send_suggestion:
                intent = new Intent(this,ComplaintSuggestionSendActivity.class);
                intent.putExtra(ComplaintSuggestionSendActivity.MESSAGE_TYPE_ARG, Constant.SUGGESTION);
                intent.putExtra(ComplaintSuggestionSendActivity.ACTIVITY_TITLE,getString(R.string.send_suggestion));
                break;

            case R.id.send_messages_absence_justification:
                intent = new Intent(this,AbsenceJustificationActivity.class);
                intent.putExtra(ComplaintSuggestionSendActivity.MESSAGE_TYPE_ARG, Constant.ABSENCES_JUST);
                break;
        }
        startActivityForResult(intent,SEND_MESSAGE_REQ);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK){
            setResult(RESULT_OK);
            finish();
        }
    }

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.send_messages_send_complaint)
    RelativeLayout sendComplaintRelativeLayout;

    @BindView(R.id.send_messages_send_suggestion)
    RelativeLayout sendSuggestionRelativeLayout;

    @BindView(R.id.send_messages_absence_justification)
    RelativeLayout absenceJustificationRelativeLayout;
}

