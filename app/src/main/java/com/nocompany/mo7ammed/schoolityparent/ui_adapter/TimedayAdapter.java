package com.nocompany.mo7ammed.schoolityparent.ui_adapter;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nocompany.mo7ammed.schoolityparent.R;
import com.nocompany.mo7ammed.schoolityparent.model.Period;

import java.util.Collections;
import java.util.List;

public class TimeDayAdapter extends RecyclerView.Adapter<TimeDayAdapter.PeriodViewHolder> {

    private List<Period> periodList;

    public TimeDayAdapter(List<Period> periods) {
        this.periodList = periods;

        Collections.sort(periodList);
    }

    @Override
    public PeriodViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.period_item, parent, false);
        return new PeriodViewHolder(v);
    }

    @Override
    public void onBindViewHolder(PeriodViewHolder holder, int position) {
        Period period = periodList.get(position);

        holder.periodOrder.setText(String.format(holder.periodOrder.getText().toString(), period.getOrder()));
        holder.periodSubjectName.setText(period.getSubject());
    }

    @Override
    public int getItemCount() {
        return periodList.size();
    }

    class PeriodViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        TextView periodOrder;
        TextView periodSubjectName;


        public PeriodViewHolder(View itemView) {
            super(itemView);
            periodOrder = (TextView) itemView.findViewById(R.id.period_item_order_tv);
            periodSubjectName = (TextView) itemView.findViewById(R.id.period_item_subject_tv);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {
            final int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {

            }
        }

        @Override
        public boolean onLongClick(View v) {
            final int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {

            }
            return true;
        }
    }
}
