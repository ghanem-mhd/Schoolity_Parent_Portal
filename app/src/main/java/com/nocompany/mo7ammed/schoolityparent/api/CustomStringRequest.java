package com.nocompany.mo7ammed.schoolityparent.api;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.nocompany.mo7ammed.schoolityparent.model.UserInfo;
import com.nocompany.mo7ammed.schoolityparent.utilities.PrefsHelper;

import java.util.HashMap;
import java.util.Map;


public class CustomStringRequest extends StringRequest {
    private boolean isAuth;

    public CustomStringRequest(int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener, boolean isAuth) {
        super(method, url, listener, errorListener);
        this.isAuth = isAuth;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json");
        headers.put("Content-Type", "application/json");

        if (isAuth) {
            UserInfo userInfo = PrefsHelper.getUserInfo();
            headers.put("X-User-Email", userInfo.getEmail());
            headers.put("X-User-Token", userInfo.getAuthentication_token());
        }

        return headers;
    }
}
