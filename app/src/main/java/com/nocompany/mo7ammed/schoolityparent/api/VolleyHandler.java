package com.nocompany.mo7ammed.schoolityparent.api;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.nocompany.mo7ammed.schoolityparent.BuildConfig;

public class VolleyHandler {

    protected static final String TAG = "VolleyHandler";
    private static final int SOCKET_TIME_OUT_MS = 10000;
    private static final int RETRY = 2;

    private RequestQueue requestQ;

    public VolleyHandler(Context context) {
        requestQ = VolleySingleton.getInstance(context.getApplicationContext()).getRequestQueue();
    }

    public static void handleVolleyError(VolleyError error) {
        if (BuildConfig.DEBUG) {
            if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                Log.d(TAG, "TimeoutError, NoConnectionError");
            } else if (error instanceof AuthFailureError) {
                Log.d(TAG, "AuthFailureError");
            } else if (error instanceof ServerError) {
                Log.d(TAG, "ServerError");
            } else if (error instanceof NetworkError) {
                Log.d(TAG, "NetworkError");
            } else if (error instanceof ParseError) {
                Log.d(TAG, "ParseError");
                Log.d(TAG, error.toString());
                Log.d(TAG, error.getMessage());
            } else
                Log.d(TAG, "There is Error");
        }
    }

    public void connect(com.android.volley.Request request) {
        request.setRetryPolicy(new DefaultRetryPolicy(SOCKET_TIME_OUT_MS, RETRY, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQ.add(request);
    }
}
