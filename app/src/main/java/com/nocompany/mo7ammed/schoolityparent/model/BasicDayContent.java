package com.nocompany.mo7ammed.schoolityparent.model;


import java.io.Serializable;

public class BasicDayContent implements Serializable{
    private String summary;

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    @Override
    public String toString() {
        return "BasicDayContent{" +
                "summary='" + summary + '\'' +
                '}';
    }
}
