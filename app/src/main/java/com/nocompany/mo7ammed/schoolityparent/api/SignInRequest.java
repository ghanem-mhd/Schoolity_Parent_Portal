package com.nocompany.mo7ammed.schoolityparent.api;


import android.content.Context;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.nocompany.mo7ammed.schoolityparent.BuildConfig;
import com.nocompany.mo7ammed.schoolityparent.model.SignInObject;

import org.json.JSONException;
import org.json.JSONObject;

public abstract class SignInRequest extends Request {
    private static final String TAG = "SignInRequest";
    private static final String URI = Request.REST_SERVER + "sign_in";
    private static final int METHOD = Request.POST;
    private SignInObject signInObject;
    private JSONObject requestBody;

    private Response.Listener<JSONObject> jsonObjectListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            SignInRequest.this.doOnResponse(response);
        }
    };
    private Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            SignInRequest.this.doOnError(error);
        }
    };

    public SignInRequest(Context context, SignInObject signInObject) {
        this.isAuthRequest = false;
        this.signInObject = signInObject;
        conHandler = new VolleyHandler(context);
        requestBody = getJsonObjectBody();
    }

    @Override
    public void request(boolean withCaching) {
        CustomJsonObjectRequest customJsonObjectRequest = new CustomJsonObjectRequest(
                METHOD,
                URI,
                requestBody,
                jsonObjectListener,
                errorListener,
                false
        );

        if (BuildConfig.DEBUG)
            Log.d(TAG, "making a request Method:" + METHOD + " URI:" + URI + " BODY:" + requestBody);
        conHandler.connect(customJsonObjectRequest);
    }

    private JSONObject getJsonObjectBody() {
        Gson gson = new Gson();
        try {
            return new JSONObject(gson.toJson(new SignInReqBody(signInObject)));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected abstract void doOnResponse(JSONObject jsonObject);

    class SignInReqBody {
        SignInObject user;
        public SignInReqBody(SignInObject signInObject) {
            this.user = signInObject;
        }
    }
}
