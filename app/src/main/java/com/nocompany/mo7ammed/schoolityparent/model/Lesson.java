package com.nocompany.mo7ammed.schoolityparent.model;


import java.io.Serializable;

public class Lesson implements Serializable{

    private int id;
    private String subject;
    private int order;
    private String content_type;
    private BasicLessonContent content;
    private Activity activity;
    private Behavior behavior;
    private int comments_count;

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public Behavior getBehavior() {
        return behavior;
    }

    public void setBehavior(Behavior behavior) {
        this.behavior = behavior;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public BasicLessonContent getContent() {
        return content;
    }

    public void setContent(BasicLessonContent content) {
        this.content = content;
    }

    public String getContent_type() {
        return content_type;
    }

    public void setContent_type(String content_type) {
        this.content_type = content_type;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getComments_count() {
        return comments_count;
    }

    public void setComments_count(int comments_count) {
        this.comments_count = comments_count;
    }

    @Override
    public String toString() {
        return "Lesson{" +
                "id=" + id +
                ", subject='" + subject + '\'' +
                ", order=" + order +
                ", content_type='" + content_type + '\'' +
                ", content=" + content +
                ", activity=" + activity +
                ", behavior=" + behavior +
                ", comments_count=" + comments_count +
                '}';
    }
}
