package com.nocompany.mo7ammed.schoolityparent.ui_adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.BootstrapLabel;
import com.nocompany.mo7ammed.schoolityparent.R;
import com.nocompany.mo7ammed.schoolityparent.model.Message;

import java.util.ArrayList;
import java.util.List;

public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.MessagesAdapterViewHolder> {

    private Context context;
    private List<Message> messageList;
    private MessagesAdapterInterface messagesAdapterInterface;

    public MessagesAdapter(Context context){
        this.context = context;
        messageList = new ArrayList<>();
    }

    public void setMessagesAdapterInterface(MessagesAdapterInterface messagesAdapterInterface) {
        this.messagesAdapterInterface = messagesAdapterInterface;
    }

    @Override
    public MessagesAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.message_item, parent, false);
        return new MessagesAdapterViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MessagesAdapterViewHolder holder, int position) {
        Message message = messageList.get(position);


        holder.commentsCount.setText(message.getComments_count()+"");
        holder.textTextView.setText(message.getText());
        holder.typeTextView.setText(message.getType(context));
    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }


    public void setNewData(List<Message> newData){
        messageList.clear();
        messageList.addAll(newData);
        notifyDataSetChanged();
    }

    class MessagesAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView typeTextView;
        TextView textTextView;
        BootstrapLabel commentsCount;

        public MessagesAdapterViewHolder(View itemView) {
            super(itemView);


            typeTextView = (TextView) itemView.findViewById(R.id.message_item_type);
            textTextView = (TextView) itemView.findViewById(R.id.message_item_text);
            commentsCount= (BootstrapLabel) itemView.findViewById(R.id.message_item_comment_count);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            final int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION && messagesAdapterInterface!= null) {
                messagesAdapterInterface.onMessageClick(messageList.get(position), position);
            }
        }
    }

    public interface MessagesAdapterInterface{
        void onMessageClick(Message message,int position);
    }
}
