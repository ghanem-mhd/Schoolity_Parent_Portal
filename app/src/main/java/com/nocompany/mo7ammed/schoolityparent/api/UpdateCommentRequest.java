package com.nocompany.mo7ammed.schoolityparent.api;


import android.content.Context;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.nocompany.mo7ammed.schoolityparent.BuildConfig;

import org.json.JSONException;
import org.json.JSONObject;

public abstract class UpdateCommentRequest extends Request {
    private static final String TAG = "UpdateCommentRequest";
    private static final int METHOD = Request.POST;
    private String URI = Request.REST_SERVER + "comments/%s";
    private JSONObject requestBody;

    private Response.Listener<JSONObject> jsonObjectListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            UpdateCommentRequest.this.doOnResponse(response);
        }
    };
    private Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            UpdateCommentRequest.this.doOnError(error);
        }
    };

    public UpdateCommentRequest(Context context, int commentID, String body) {
        this.isAuthRequest = true;
        URI = String.format(URI, commentID);
        conHandler = new VolleyHandler(context);
        requestBody = getJsonObjectBody(body);
    }

    @Override
    public void request(boolean withCaching) {
        CustomJsonObjectRequest customJsonObjectRequest = new CustomJsonObjectRequest(
                METHOD,
                URI,
                requestBody,
                jsonObjectListener,
                errorListener,
                isAuthRequest
        );

        if (BuildConfig.DEBUG)
            Log.d(TAG, "making a request Method:" + METHOD + " URI:" + URI + " BODY:" + requestBody);
        conHandler.connect(customJsonObjectRequest);
    }

    private JSONObject getJsonObjectBody(String body) {
        Gson gson = new Gson();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("comment", new JSONObject(gson.toJson(new AddCommentBody(body))));
            return jsonObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected abstract void doOnResponse(JSONObject jsonObject);

    class AddCommentBody {
        String body;

        public AddCommentBody(String body) {
            this.body = body;
        }
    }
}

