package com.nocompany.mo7ammed.schoolityparent.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


public class Timetable {
    private int id;
    private Classroom classroom;
    private List<String> weekends;
    private int periods_number;
    private List<Period> periods;


    @Override
    public String toString() {
        return "Timetable{" +
                "id=" + id +
                ", classroom=" + classroom +
                ", weekends=" + weekends +
                ", periods_number=" + periods_number +
                ", periods=" + periods +
                '}';
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Classroom getClassroom() {
        return classroom;
    }

    public void setClassroom(Classroom classroom) {
        this.classroom = classroom;
    }

    public int getPeriods_number() {
        return periods_number;
    }

    public void setPeriods_number(int periods_number) {
        this.periods_number = periods_number;
    }

    public List<Period> getPeriods() {
        return periods;
    }

    public void setPeriods(List<Period> periods) {
        this.periods = periods;
    }

    public List<String> getWeekends() {
        return weekends;
    }

    public void setWeekends(List<String> weekends) {
        this.weekends = weekends;
    }

    public List<TimetableDay> getTimetableDays() {
        Map<String, List<Period>> map = new TreeMap<>();
        for (Period period : periods) {
            List<Period> list = map.get(period.getDay());
            if (list != null) {
                list.add(period);
            } else {
                list = new ArrayList<>();
                list.add(period);
                map.put(period.getDay(), list);
            }
        }
        ArrayList<TimetableDay> timetableDays = new ArrayList<>();
        for (Map.Entry<String, List<Period>> entry : map.entrySet()) {
            timetableDays.add(new TimetableDay(entry.getKey(), entry.getValue()));
        }
        return timetableDays;
    }
}
