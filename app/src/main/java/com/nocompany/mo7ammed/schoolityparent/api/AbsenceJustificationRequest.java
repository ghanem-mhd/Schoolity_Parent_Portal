package com.nocompany.mo7ammed.schoolityparent.api;


import android.content.Context;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.nocompany.mo7ammed.schoolityparent.BuildConfig;
import com.nocompany.mo7ammed.schoolityparent.model.AbsenceJustificationObject;

import org.json.JSONException;
import org.json.JSONObject;

public abstract class AbsenceJustificationRequest extends Request {
    private static final String TAG = "AbsenceJusRequest";
    private String URI = Request.REST_SERVER + "parent/followings/%s/messages";
    private static final int METHOD = Request.POST;
    private JSONObject requestBody;

    private Response.Listener<JSONObject> jsonObjectListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            AbsenceJustificationRequest.this.doOnResponse(response);
        }
    };
    private Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            AbsenceJustificationRequest.this.doOnError(error);
        }
    };

    public AbsenceJustificationRequest(Context context, int followingID,AbsenceJustificationObject absenceJustificationObject) {
        this.isAuthRequest = true;
        URI = String.format(URI,followingID);
        conHandler = new VolleyHandler(context);
        requestBody = getJsonObjectBody(absenceJustificationObject);
    }

    @Override
    public void request(boolean withCaching) {
        CustomJsonObjectRequest customJsonObjectRequest = new CustomJsonObjectRequest(
                METHOD,
                URI,
                requestBody,
                jsonObjectListener,
                errorListener,
                isAuthRequest
        );

        if (BuildConfig.DEBUG)
            Log.d(TAG, "making a request Method:" + METHOD + " URI:" + URI + " BODY:" + requestBody);
        conHandler.connect(customJsonObjectRequest);
    }

    private JSONObject getJsonObjectBody(AbsenceJustificationObject absenceJustificationObject) {
        Gson gson = new Gson();
        try {
            return new JSONObject(gson.toJson(new AbsenceJustificationReqBody(absenceJustificationObject)));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected abstract void doOnResponse(JSONObject jsonObject);

    class AbsenceJustificationReqBody {
        AbsenceJustificationObject message;
        public AbsenceJustificationReqBody(AbsenceJustificationObject absenceJustificationObject) {
            this.message = absenceJustificationObject;
        }
    }
}

