package com.nocompany.mo7ammed.schoolityparent.model;

import java.text.SimpleDateFormat;
import java.util.Date;


public class Comment {

    public static final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

    private int id;
    private String user;
    private String role;
    private String body;
    private String created_at;
    private String updated_at;
    private Boolean owner;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public Date getUpdateData() {
        try {
            return format.parse(updated_at);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Date getCreateData() {
        try {
            return format.parse(created_at);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }


    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", user='" + user + '\'' +
                ", role='" + role + '\'' +
                ", body='" + body + '\'' +
                ", created_at='" + created_at + '\'' +
                ", updated_at='" + updated_at + '\'' +
                ", owner=" + owner +
                '}';
    }

    public Boolean getOwner() {
        return owner;
    }

    public void setOwner(Boolean owner) {
        this.owner = owner;
    }
}
