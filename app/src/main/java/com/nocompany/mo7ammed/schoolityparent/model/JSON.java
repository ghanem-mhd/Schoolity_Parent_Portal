package com.nocompany.mo7ammed.schoolityparent.model;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JSON {
    private int id;
    private String url;
    private String value;

    public JSON() {
    }

    public JSON(String url, String value) {
        this.url = url;
        this.value = value;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "JSON{" +
                "id=" + id +
                ", url='" + url + '\'' +
                ", value='" + value + '\'' +
                '}';
    }


    public JSONObject getJsonObject(){
        try {
            return new JSONObject(value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public JSONArray getJsonArray(){
        try {
            return new JSONArray(value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
