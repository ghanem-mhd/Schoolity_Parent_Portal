package com.nocompany.mo7ammed.schoolityparent.db_cache;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.nocompany.mo7ammed.schoolityparent.model.JSON;

import java.util.ArrayList;
import java.util.List;

public class JSONsDatabaseHelper extends SQLiteOpenHelper {

    private static final String TAG = "JSONsDatabaseHelper";

    private static final String DATABASE_NAME = "JSONsDatabase";
    private static final int DATABASE_VERSION = 1;

    private static final String TABLE_JSONs = "JSONs";

    private static final String KEY_JSON_ID = "id";
    private static final String KEY_JSON_URI = "uri";
    private static final String KEY_POST_VALUE = "value";


    private static JSONsDatabaseHelper sInstance;

    private JSONsDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static synchronized JSONsDatabaseHelper getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new JSONsDatabaseHelper(context.getApplicationContext());
        }
        return sInstance;
    }

    public static synchronized JSONsDatabaseHelper getInstance() {
        if (sInstance == null) {
            throw new IllegalAccessError("Call getInstance with context first");
        }
        return sInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_JSONs_TABLE = "CREATE TABLE " + TABLE_JSONs +
                "(" +
                KEY_JSON_ID + " INTEGER PRIMARY KEY," +
                KEY_JSON_URI + " TEXT UNIQUE, " +
                KEY_POST_VALUE + " TEXT" +
                ")";
        db.execSQL(CREATE_JSONs_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion != newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_JSONs);
            onCreate(db);
        }
    }

    public void addJSON(JSON json) {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_JSON_URI, json.getUrl());
            values.put(KEY_POST_VALUE, json.getValue());

            if (json.getValue() != null && !json.getValue().isEmpty()) {
                db.insertWithOnConflict(TABLE_JSONs, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                db.setTransactionSuccessful();
            }
        } catch (Exception e) {
            Log.d(TAG, "Error while trying to add json to database");
        } finally {
            db.endTransaction();
        }
    }


    public JSON getJSON(String uri) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_JSONs,
                new String[]{KEY_JSON_ID, KEY_JSON_URI, KEY_POST_VALUE},
                KEY_JSON_URI + " = ?",
                new String[]{uri},
                null,
                null,
                null);
        try {
            if (cursor.moveToFirst()) {
                JSON json = new JSON();
                json.setId(cursor.getInt(cursor.getColumnIndex(KEY_JSON_ID)));
                json.setUrl(cursor.getString(cursor.getColumnIndex(KEY_JSON_URI)));
                json.setValue(cursor.getString(cursor.getColumnIndex(KEY_POST_VALUE)));
                return json;
            }
        } catch (Exception e) {
            Log.d(TAG, "Error while trying to get posts from database");
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return null;
    }

    public List<JSON> getAllJSON() {
        List<JSON> JSONs = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_JSONs,
                new String[]{KEY_JSON_ID, KEY_JSON_URI, KEY_POST_VALUE},
                null,
                null,
                null,
                null,
                null);
        try {
            if (cursor.moveToFirst()) {
                do {
                    JSON json = new JSON();
                    json.setId(cursor.getInt(cursor.getColumnIndex(KEY_JSON_ID)));
                    json.setUrl(cursor.getString(cursor.getColumnIndex(KEY_JSON_URI)));
                    json.setValue(cursor.getString(cursor.getColumnIndex(KEY_POST_VALUE)));
                    JSONs.add(json);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.d(TAG, "Error while trying to get posts from database");
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return JSONs;
    }

    public void clearCache(){
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_JSONs,null,null);
    }
}