package com.nocompany.mo7ammed.schoolityparent.model;


import android.os.Parcel;
import android.os.Parcelable;

public class Period implements Parcelable, Comparable<Period> {
    public static final Creator<Period> CREATOR = new Creator<Period>() {
        @Override
        public Period createFromParcel(Parcel in) {
            return new Period(in);
        }

        @Override
        public Period[] newArray(int size) {
            return new Period[size];
        }
    };
    String day;
    int order;
    String subject;

    protected Period(Parcel in) {
        day = in.readString();
        order = in.readInt();
        subject = in.readString();
    }

    @Override
    public String toString() {
        return "Period{" +
                "day='" + day + '\'' +
                ", order=" + order +
                ", subject='" + subject + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(day);
        dest.writeInt(order);
        dest.writeString(subject);
    }


    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Override
    public int compareTo(Period another) {
        return order < another.getOrder() ? -1 : (order == another.getOrder() ? 0 : 1);
    }
}