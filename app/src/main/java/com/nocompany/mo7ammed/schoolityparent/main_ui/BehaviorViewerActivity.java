package com.nocompany.mo7ammed.schoolityparent.main_ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.nocompany.mo7ammed.schoolityparent.R;
import com.nocompany.mo7ammed.schoolityparent.model.Behavior;
import com.nocompany.mo7ammed.schoolityparent.utilities.Constant;

public class BehaviorViewerActivity extends AppCompatActivity {
    public static final String NOTIFICATION_SUMMARY = "notificationSummary";
    public static final String BEHAVIOR_ARG = "behaviorObject";
    private Behavior behavior;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_behavior_viwer);

        behavior = (Behavior) getIntent().getSerializableExtra(BEHAVIOR_ARG);

        setUpToolbar();

        setBehaviorToUI();

        setNotificationSummary();
    }

    private void setNotificationSummary() {
        CardView cardView = (CardView) findViewById(R.id.notification_summary_cardView);
        TextView textView = (TextView) findViewById(R.id.notification_summary);
        String notificationSummary = getIntent().getStringExtra(NOTIFICATION_SUMMARY);
        if (notificationSummary != null && !notificationSummary.isEmpty()){
            cardView.setVisibility(View.VISIBLE);
            textView.setText(notificationSummary);
        }
    }


    private void setBehaviorToUI() {
        TextView behaviorRatingTV    = (TextView) findViewById(R.id.lesson_viewer_behavior_rate);
        TextView behaviorNotes       = (TextView) findViewById(R.id.lesson_viewer_behavior_notes);
        assert behaviorRatingTV     != null;
        assert behaviorNotes        != null;


        behaviorRatingTV.setText(behavior.getContent().getRating());
        behaviorNotes.setText(behavior.getContent().getNotes());
    }

    private void setUpToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        assert toolbar != null;
        toolbar.setTitle("Student Behavior");
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(RESULT_CANCELED);
                finish();
                return true;

            case R.id.action_menu_discuss:
                Intent intent = new Intent(this, CommentsActivity.class);
                intent.putExtra(CommentsActivity.ID_ARG, behavior.getId());
                intent.putExtra(CommentsActivity.TYPE_ARG, Constant.BEHAVIORS);
                intent.putExtra(CommentsActivity.TITLE_ARG,getString(R.string.behavior));
                startActivity(intent);
                break;
        }
        return false;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (behavior.getId() != 0){
            getMenuInflater().inflate(R.menu.discuss_menu, menu);
        }
        return true;
    }
}
