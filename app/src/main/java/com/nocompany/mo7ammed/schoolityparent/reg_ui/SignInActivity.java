package com.nocompany.mo7ammed.schoolityparent.reg_ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

import com.android.volley.VolleyError;
import com.nocompany.mo7ammed.schoolityparent.R;
import com.nocompany.mo7ammed.schoolityparent.api.SignInRequest;
import com.nocompany.mo7ammed.schoolityparent.main_ui.MainActivity;
import com.nocompany.mo7ammed.schoolityparent.model.Error;
import com.nocompany.mo7ammed.schoolityparent.model.JSONParser;
import com.nocompany.mo7ammed.schoolityparent.model.SignInObject;
import com.nocompany.mo7ammed.schoolityparent.model.UserInfo;
import com.nocompany.mo7ammed.schoolityparent.utilities.App;
import com.nocompany.mo7ammed.schoolityparent.utilities.BaseActivity;
import com.nocompany.mo7ammed.schoolityparent.utilities.PrefsHelper;
import com.nocompany.mo7ammed.schoolityparent.utilities.PrefsKeys;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SignInActivity extends BaseActivity {

    @BindView(R.id.signIn_email_et)
    EditText emailEditText;
    @BindView(R.id.signIn_password_et)
    EditText passwordEditText;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signin_activity);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
    }

    public void signInClickListener(View view) {
        App.hideSoftKeyboard(this);

        SignInObject signInObject = new SignInObject();
        signInObject.setEmail(emailEditText.getText().toString());
        signInObject.setPassword(passwordEditText.getText().toString());

        int result = signInObject.checkData();
        if (result == -1)
            API_SignIn_Request(signInObject);
        else
            showLongToast(getString(result));

    }

    public void signUpClickListener(View view) {
        Intent intent = new Intent(this, SignUpActivity.class);
        startActivity(intent);
    }

    public void facebookClickListener(View view) {

    }

    private void API_SignIn_Request(SignInObject signInObject) {
        showLoadingDialog();
        SignInRequest signInRequest = new SignInRequest(getApplicationContext(), signInObject) {
            @Override
            protected void doOnResponse(JSONObject jsonObject) {
                API_SignIn_Response(jsonObject);
            }

            @Override
            protected void doOnError(VolleyError error) {
                API_SignIn_Error(new Error(SignInActivity.this.getApplicationContext(), error));
            }
        };
        signInRequest.request(false);
    }

    private void API_SignIn_Response(JSONObject jsonObject) {
        dismissLoadingDialog();
        UserInfo userInfo = JSONParser.newUserInfo(jsonObject);
        finishSigningIn(userInfo);
    }

    private void API_SignIn_Error(Error error) {
        dismissLoadingDialog();
        showLongToast(error.getErrorMessage());
    }

    private void finishSigningIn(UserInfo userInfo) {
        debugLog(userInfo.toString());

        Prefs.putBoolean(PrefsKeys.IS_LOGIN, true);
        PrefsHelper.saveUserInfo(userInfo);


        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
}
