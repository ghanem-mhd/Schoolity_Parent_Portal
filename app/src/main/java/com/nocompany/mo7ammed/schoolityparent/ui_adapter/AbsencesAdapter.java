package com.nocompany.mo7ammed.schoolityparent.ui_adapter;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nocompany.mo7ammed.schoolityparent.R;
import com.nocompany.mo7ammed.schoolityparent.model.Absence;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class AbsencesAdapter extends RecyclerView.Adapter<AbsencesAdapter.AbsenceViewHolder>{

    private DateFormat originalFormat   = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
    private DateFormat dateFormat       = new SimpleDateFormat("EEEE dd MMMM yyyy",Locale.ENGLISH);


    private List<Absence> absences;
    private AbsencesAdapterInterface absencesAdapterInterface;
    public AbsencesAdapter() {
        this.absences = new ArrayList<>();
    }


    public void setAbsencesAdapterInterface(AbsencesAdapterInterface absencesAdapterInterface) {
        this.absencesAdapterInterface = absencesAdapterInterface;
    }

    @Override
    public AbsenceViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.absence_item, viewGroup, false);
        return new AbsenceViewHolder(v);
    }

    @Override
    public void onBindViewHolder(AbsenceViewHolder holder, int position) {
        Absence absence = absences.get(position);
        try {
            Date date = originalFormat.parse(absence.getDate());
            holder.dateTextView.setText(dateFormat.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
            holder.dateTextView.setText(absence.getDate());
        }
    }

    @Override
    public int getItemCount() {
        return absences.size();
    }

    public void setNewData(List<Absence> newAbsencesList){
        absences.clear();
        absences.addAll(newAbsencesList);
        notifyDataSetChanged();
    }

    class AbsenceViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView dateTextView;

        public AbsenceViewHolder(View itemView) {
            super(itemView);
            dateTextView = (TextView) itemView.findViewById(R.id.absence_item_date);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            final int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION && absencesAdapterInterface != null) {
                absencesAdapterInterface.onAbsenceClick(absences.get(position), position);
            }
        }
    }

    public interface AbsencesAdapterInterface{
        void onAbsenceClick(Absence absence,int position);
    }
}
