package com.nocompany.mo7ammed.schoolityparent.model;


import com.nocompany.mo7ammed.schoolityparent.R;

import java.io.Serializable;

public class DateYMD implements Serializable{
    private int year;
    private int month;
    private int day;


    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }


    public int checkData() {
        if (year == -1 || month == -1 || day ==-1) {
            return R.string.check_date;
        }
        return -1;
    }
}
