package com.nocompany.mo7ammed.schoolityparent.ui_adapter;


import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentStatePagerAdapter;

import com.nocompany.mo7ammed.schoolityparent.main_ui.TimetableDayFragment;
import com.nocompany.mo7ammed.schoolityparent.model.TimetableDay;

import java.util.List;

public class TimetableAdapter extends FragmentStatePagerAdapter {

    private List<TimetableDay> timetableDays;

    public TimetableAdapter(FragmentManager fragmentManager, List<TimetableDay> timetableDays) {
        super(fragmentManager);
        this.timetableDays = timetableDays;
    }

    @Override
    public int getCount() {
        return timetableDays.size();
    }


    @Override
    public Fragment getItem(int position) {
        return TimetableDayFragment.newInstance(timetableDays.get(position));
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return timetableDays.get(position).getDayName();
    }
}
