package com.nocompany.mo7ammed.schoolityparent.main_ui;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.mugen.Mugen;
import com.mugen.MugenCallbacks;
import com.nocompany.mo7ammed.schoolityparent.R;
import com.nocompany.mo7ammed.schoolityparent.api.FeedRequest;
import com.nocompany.mo7ammed.schoolityparent.api.FollowingStudentRequest;
import com.nocompany.mo7ammed.schoolityparent.db_cache.JSONsDatabaseHelper;
import com.nocompany.mo7ammed.schoolityparent.helper.SimpleDividerItemDecoration;
import com.nocompany.mo7ammed.schoolityparent.model.Announcement;
import com.nocompany.mo7ammed.schoolityparent.model.Feedable;
import com.nocompany.mo7ammed.schoolityparent.model.Grade;
import com.nocompany.mo7ammed.schoolityparent.model.JSON;
import com.nocompany.mo7ammed.schoolityparent.model.JSONParser;
import com.nocompany.mo7ammed.schoolityparent.ui_adapter.FeedsAdapter;
import com.nocompany.mo7ammed.schoolityparent.utilities.BaseFragment;

import org.json.JSONArray;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FeedsFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, FeedsAdapter.FeedsAdapterInterface,MugenCallbacks{

    @BindView(R.id.feeds_recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.feeds_swipeRefresh)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.feeds_error_textView)
    TextView errorTextView;

    @BindView(R.id.feeds_error_scrollView)
    ScrollView scrollView;

    private int current_page = 1;
    private Boolean isLoading = false;
    private Integer numberOfNewItems = -1;

    private FeedsAdapter feedsAdapter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.feed_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setUpToolBar();

        setUpAdsList();

        API_Feed_Request(true);
    }

    private void setUpToolBar() {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.main_toolbar);
        toolbar.setTitle(R.string.news_feed);
    }

    private void setUpAdsList() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        feedsAdapter = new FeedsAdapter(getActivity());

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(feedsAdapter);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity().getApplicationContext()));

        Mugen.with(recyclerView, this).start();

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getActivity(), R.color.colorAccent));

        feedsAdapter.setFeedsAdapterInterface(this);
    }

    private void API_Feed_Request(boolean fromCache) {
        if (!checkFollowing()){
            setRefreshing(false);
            showWarningTextView(getString(R.string.no_following_student_go));
            return;
        }
        setRefreshing(true);
        FeedRequest feedRequest = new FeedRequest(getActivity(),current_page) {
            @Override
            protected void doOnResponse(JSONArray jsonArray) {
                API_Feed_Response(jsonArray);
            }

            @Override
            protected void doOnError(VolleyError error) {
                API_Feed_Error(error);
            }
        };
        feedRequest.request(fromCache);
    }

    private void API_Feed_Response(JSONArray jsonArray) {
        setRefreshing(false);
        List<Feedable> feedableList = JSONParser.parseFeed(jsonArray);
        if (feedableList != null && feedableList.size() > 0) {
            scrollView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            numberOfNewItems = feedsAdapter.appendData(feedableList);
        } else {
            if (current_page == 1){
                showWarningTextView(getString(R.string.no_grades));
            }else {
                numberOfNewItems = 0;
            }
        }
    }

    private void API_Feed_Error(VolleyError volleyError) {
        setRefreshing(false);
        On_API_Error(volleyError, new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void showWarningTextView(String warningMessage) {
        recyclerView.setVisibility(View.GONE);
        scrollView.setVisibility(View.VISIBLE);
        errorTextView.setText(warningMessage);
    }


    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                API_Feed_Request(false);
            }
        }, 500);
    }


    private void setRefreshing(final boolean isRefreshing) {
        isLoading = isRefreshing;
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(isRefreshing);
            }
        });
    }

    @Override
    public void onFeedsItemClick(Feedable feedable, int position) {
        if (feedable instanceof Announcement){
            Intent intent = new Intent(getActivity(),AnnouncementViewerActivity.class);
            intent.putExtra(AnnouncementViewerActivity.ANNOUNCEMENT_OBJECT_ARG,(Announcement)feedable);
            startActivity(intent);
        }

        if (feedable instanceof Grade){
            Intent intent = new Intent(getActivity(),GradeViewerActivity.class);
            intent.putExtra(GradeViewerActivity.Grade_OBJECT_ARG,(Grade)feedable);
            startActivity(intent);
        }
    }

    @Override
    public void onFeedsItemLongPress(View view, Feedable feedable, int position) {

    }

    @Override
    public void onLoadMore() {
        current_page++;
        API_Feed_Request(true);
    }

    @Override
    public boolean isLoading() {
        return isLoading;
    }

    @Override
    public boolean hasLoadedAllItems() {
        return numberOfNewItems == 0;
    }


    private boolean checkFollowing() {
        JSONsDatabaseHelper jsoNsDatabaseHelper = JSONsDatabaseHelper.getInstance();
        JSON followedStudentJSON = jsoNsDatabaseHelper.getJSON(FollowingStudentRequest.URI);
        return followedStudentJSON != null && followedStudentJSON.getJsonArray().length() > 0;
    }
}

