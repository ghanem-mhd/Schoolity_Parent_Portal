package com.nocompany.mo7ammed.schoolityparent.model;


import com.nocompany.mo7ammed.schoolityparent.R;

public class SignUpObject {
    private String email;
    private String password;
    private String first_name;
    private String last_name;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public int checkData() {
        if (email == null || email.isEmpty()) {
            return R.string.check_email;
        }
        if (password == null || password.isEmpty()) {
            return R.string.check_password;
        }
        if (first_name == null || first_name.isEmpty()) {
            return R.string.check_first_name;
        }

        if (last_name == null || last_name.isEmpty()) {
            return R.string.check_last_name;
        }
        return -1;
    }
}
