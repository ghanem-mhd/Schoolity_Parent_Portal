package com.nocompany.mo7ammed.schoolityparent.ui_adapter;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.nocompany.mo7ammed.schoolityparent.R;
import com.nocompany.mo7ammed.schoolityparent.model.Announcement;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class AnnouncementsAdapter extends RecyclerView.Adapter<AnnouncementsAdapter.AnnouncementsAdapterViewHolder> implements Filterable {

    private AnnouncementsAdapterInterface announcementsAdapterInterface;
    private List<Announcement> filteredAnnouncementList;
    private List<Announcement> announcementList;
    private AnnouncementsFilter announcementsFilter;

    public AnnouncementsAdapter() {
        announcementList = new ArrayList<>();
        filteredAnnouncementList = new ArrayList<>();
    }

    @Override
    public AnnouncementsAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.announcement_item, parent, false);
        return new AnnouncementsAdapterViewHolder(v);
    }

    @Override
    public void onBindViewHolder(AnnouncementsAdapterViewHolder holder, int position) {
        Announcement announcement = filteredAnnouncementList.get(position);

        holder.announcementTitle.setText(announcement.getTitle());
        holder.announcementType.setText(announcement.getAnnounceable_type());
    }

    @Override
    public int getItemCount() {
        if (filteredAnnouncementList != null)
            return filteredAnnouncementList.size();
        else
            return 0;
    }

    public void setAnnouncementsAdapterInterface(AnnouncementsAdapterInterface announcementsAdapterInterface) {
        this.announcementsAdapterInterface = announcementsAdapterInterface;
    }

    public void setNewData(List<Announcement> newAnnouncements) {
        announcementList.clear();
        filteredAnnouncementList.clear();
        if (newAnnouncements != null) {
            announcementList.addAll(newAnnouncements);
            filteredAnnouncementList.addAll(newAnnouncements);
        }

    }

    @Override
    public Filter getFilter() {
        if (announcementsFilter == null)
            announcementsFilter = new AnnouncementsFilter(this, announcementList);
        return announcementsFilter;
    }


    public interface AnnouncementsAdapterInterface {
        void onAnnouncementClick(Announcement announcement, int position);

        void onAnnouncementLongPress(View view, Announcement announcement, int position);
    }

    private static class AnnouncementsFilter extends Filter {

        private final AnnouncementsAdapter adapter;

        private final List<Announcement> originalList;

        private final List<Announcement> filteredList;

        private AnnouncementsFilter(AnnouncementsAdapter adapter, List<Announcement> originalList) {
            super();
            this.adapter = adapter;
            this.originalList = new LinkedList<>(originalList);
            this.filteredList = new ArrayList<>();
        }


        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            filteredList.clear();
            final FilterResults results = new FilterResults();

            if (constraint.length() == 0) {
                filteredList.addAll(originalList);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                for (final Announcement announcement : originalList) {
                    if (announcement.getAnnounceable_type().equalsIgnoreCase(filterPattern)) {
                        filteredList.add(announcement);
                    }
                }
            }
            results.values = filteredList;
            results.count = filteredList.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            adapter.filteredAnnouncementList.clear();
            adapter.filteredAnnouncementList.addAll((ArrayList<Announcement>) results.values);
            adapter.notifyDataSetChanged();
        }
    }

    class AnnouncementsAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        TextView announcementTitle;
        TextView announcementType;


        public AnnouncementsAdapterViewHolder(View itemView) {
            super(itemView);
            announcementTitle = (TextView) itemView.findViewById(R.id.announcement_item_title);
            announcementType = (TextView) itemView.findViewById(R.id.announcement_item_type);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {
            final int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION && announcementsAdapterInterface != null) {
                announcementsAdapterInterface.onAnnouncementClick(filteredAnnouncementList.get(position), position);
            }
        }

        @Override
        public boolean onLongClick(View v) {
            final int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION && announcementsAdapterInterface != null) {
                announcementsAdapterInterface.onAnnouncementLongPress(v, filteredAnnouncementList.get(position), position);
            }
            return true;
        }
    }
}
