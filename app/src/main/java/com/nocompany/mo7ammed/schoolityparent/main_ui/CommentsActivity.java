package com.nocompany.mo7ammed.schoolityparent.main_ui;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.melnykov.fab.FloatingActionButton;
import com.nocompany.mo7ammed.schoolityparent.R;
import com.nocompany.mo7ammed.schoolityparent.api.AddCommentReq;
import com.nocompany.mo7ammed.schoolityparent.api.CommentsRequest;
import com.nocompany.mo7ammed.schoolityparent.api.DeleteCommentRequest;
import com.nocompany.mo7ammed.schoolityparent.api.UpdateCommentRequest;
import com.nocompany.mo7ammed.schoolityparent.api.VolleyHandler;
import com.nocompany.mo7ammed.schoolityparent.helper.SimpleDividerItemDecoration;
import com.nocompany.mo7ammed.schoolityparent.model.Comment;
import com.nocompany.mo7ammed.schoolityparent.model.JSONParser;
import com.nocompany.mo7ammed.schoolityparent.ui_adapter.CommentsAdapter;
import com.nocompany.mo7ammed.schoolityparent.utilities.BaseActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CommentsActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener, CommentsAdapter.CommentsAdapterInterface {


    public final static String TITLE_ARG    = "title_arg";
    public final static String ID_ARG       = "id_arg";
    public final static String TYPE_ARG     = "type_arg";

    private String commentableType;
    private int commentableID;
    private CommentsAdapter commentsAdapter;

    private int updatedCommentID = -1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.comments_activity);
        ButterKnife.bind(this);

        commentableType = getIntent().getStringExtra(TYPE_ARG);
        commentableID = getIntent().getIntExtra(ID_ARG, -1);

        setUpToolbar();

        setUpAdsList();

        API_Comments_Request(false);
    }

    private void setUpToolbar() {
        String title = getIntent().getStringExtra(TITLE_ARG);
        if (title!= null && !title.isEmpty()){
            toolbar.setTitle(title +" "+getString(R.string.discussion));
        }else {
            toolbar.setTitle(getString(R.string.discussion));
        }

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setUpAdsList() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        commentsAdapter = new CommentsAdapter();

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(commentsAdapter);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(this));

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(this, R.color.colorAccent));

        commentsAdapter.setCommentsAdapterInterface(this);
    }

    private void API_Comments_Request(boolean fromCache) {
        setRefreshing(true);
        CommentsRequest commentsRequest = new CommentsRequest(this, commentableType, commentableID) {
            @Override
            protected void doOnResponse(JSONArray jsonArray) {
                API_Comments_Response(jsonArray);
            }

            @Override
            protected void doOnError(VolleyError error) {
                VolleyHandler.handleVolleyError(error);
                API_Comments_Error(error);
            }


        };
        commentsRequest.request(fromCache);
    }

    private void API_Comments_Error(VolleyError error) {
        setRefreshing(false);
        On_API_Error(error, new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void API_Comments_Response(JSONArray jsonArray) {
        setRefreshing(false);
        List<Comment> comments = JSONParser.getCommentsList(jsonArray);
        if (comments != null && comments.size() > 0) {
            scrollView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);

            commentsAdapter.setNewData(comments);
            recyclerView.swapAdapter(commentsAdapter, true);

        } else {
            showWarningTextView(getString(R.string.no_comments));
        }
    }

    private void showWarningTextView(String warningMessage) {
        recyclerView.setVisibility(View.GONE);
        scrollView.setVisibility(View.VISIBLE);
        errorTextView.setText(warningMessage);
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                API_Comments_Request(false);
            }
        }, 500);
    }

    private void setRefreshing(final boolean isRefreshing) {
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(isRefreshing);
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(RESULT_CANCELED);
                finish();
                return true;
        }
        return false;
    }

    public void onSendButtonClick(View view) {
        String commentBody = commentInputEditText.getText().toString();
        if (commentBody.isEmpty()) {
            showLongToast(getString(R.string.add_comment));
        } else {
            API_ADD_Comment_Request(commentBody);
        }
    }

    private void API_ADD_Comment_Request(String body) {
        setRefreshing(true);

        if (updatedCommentID != -1) {
            UpdateCommentRequest updateCommentRequest = new UpdateCommentRequest(this, updatedCommentID, body) {
                @Override
                protected void doOnResponse(JSONObject jsonObject) {
                    API_ADD_Comment_Response();
                }

                @Override
                protected void doOnError(VolleyError error) {
                    API_ADD_Comment_Error();
                }
            };
            updateCommentRequest.request(false);
        } else {
            AddCommentReq addCommentReq = new AddCommentReq(this, commentableType, commentableID, body) {
                @Override
                protected void doOnResponse(JSONObject jsonObject) {
                    API_ADD_Comment_Response();
                }

                @Override
                protected void doOnError(VolleyError error) {
                    API_ADD_Comment_Error();
                }
            };
            addCommentReq.request(false);
        }


    }

    private void API_ADD_Comment_Response() {
        API_Comments_Request(false);
        commentInputEditText.setText("");
        updatedCommentID = -1;
    }

    private void API_ADD_Comment_Error() {
        setRefreshing(false);
        showLongToast(getString(R.string.can_not_add_comment));
    }

    @Override
    public void onCommentClick(Comment comment, int position) {

    }

    @Override
    public void onCommentLongPress(View view, final Comment comment, int position) {
        PopupMenu popup = new PopupMenu(this, view);
        popup.getMenuInflater().inflate(R.menu.comment_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.action_delete_comment) {
                    API_Delete_Comment_Request(comment.getId());
                    return true;
                }

                if (item.getItemId() == R.id.action_edit_comment) {
                    EditComment(comment);
                    return true;
                }

                return false;
            }
        });
        popup.show();
    }

    private void EditComment(Comment updatedComment) {
        updatedCommentID = updatedComment.getId();
        commentInputEditText.setText(updatedComment.getBody());
        showLongToast(getString(R.string.click_send_button_to_update));
    }

    private void API_Delete_Comment_Request(int commentableID) {
        setRefreshing(true);
        DeleteCommentRequest deleteCommentRequest = new DeleteCommentRequest(this, commentableID) {
            @Override
            protected void doOnResponse(String response) {
                API_Delete_Comment_Response(response);
            }

            @Override
            protected void doOnError(VolleyError error) {
                API_Delete_Comment_Error();
            }
        };
        deleteCommentRequest.request(false);
    }

    private void API_Delete_Comment_Response(String response) {
        API_Comments_Request(false);
        showLongToast(response);
    }

    private void API_Delete_Comment_Error() {
        API_Comments_Request(false);
        showLongToast(getString(R.string.can_not_delete_comment));
    }


    @BindView(R.id.comments_recyclerView) RecyclerView recyclerView;
    @BindView(R.id.comments_swipeRefresh) SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.comments_error_textView) TextView errorTextView;
    @BindView(R.id.comments_error_scrollView) ScrollView scrollView;
    @BindView(R.id.comments_sendButton) FloatingActionButton floatingSendButton;
    @BindView(R.id.comments_commentInput) EditText commentInputEditText;
    @BindView(R.id.toolbar) Toolbar toolbar;

}
