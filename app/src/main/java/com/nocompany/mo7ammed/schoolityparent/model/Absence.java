package com.nocompany.mo7ammed.schoolityparent.model;


import java.io.Serializable;

public class Absence implements Serializable{

    private int id;
    private String date;
    private int comments_count;

    public String getDate() {
        return date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getComments_count() {
        return comments_count;
    }

    public void setComments_count(int comments_count) {
        this.comments_count = comments_count;
    }

    @Override
    public String toString() {
        return "Absence{" +
                "id=" + id +
                ", date='" + date + '\'' +
                ", comments_count=" + comments_count +
                '}';
    }
}
