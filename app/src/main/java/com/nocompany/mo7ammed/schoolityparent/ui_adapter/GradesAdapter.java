package com.nocompany.mo7ammed.schoolityparent.ui_adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.BootstrapLabel;
import com.beardedhen.androidbootstrap.api.defaults.DefaultBootstrapBrand;
import com.nocompany.mo7ammed.schoolityparent.R;
import com.nocompany.mo7ammed.schoolityparent.model.Grade;

import java.util.ArrayList;
import java.util.List;


public class GradesAdapter extends RecyclerView.Adapter<GradesAdapter.GradeViewHolder> {

    private List<Grade> grades;
    private GradesAdapterInterface gradesAdapterInterface;


    public GradesAdapter() {
        grades = new ArrayList<>();
    }

    public void setGradesAdapterInterface(GradesAdapterInterface gradesAdapterInterface) {
        this.gradesAdapterInterface = gradesAdapterInterface;
    }

    @Override
    public GradeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.grade_item, parent, false);
        return new GradeViewHolder(v);
    }

    @Override
    public void onBindViewHolder(GradeViewHolder holder, int position) {
        Grade grade = grades.get(position);

        holder.examDescriptionTextView.setText(grade.getExam().getDate());
        holder.examScore.setText(String.format("%s/%s", grade.getScore(), grade.getExam().getScore()));

        try {
            if (Float.parseFloat(grade.getScore()) >= Float.parseFloat(grade.getExam().getMinimum_score())) {
                holder.examScore.setBootstrapBrand(DefaultBootstrapBrand.SUCCESS);
            } else {
                holder.examScore.setBootstrapBrand(DefaultBootstrapBrand.DANGER);
            }

        } catch (Exception ignored) {
        }

        holder.subjectNameTextView.setText(grade.getExam().getSubject().getName());
    }

    @Override
    public int getItemCount() {
        if (grades != null)
            return grades.size();
        return 0;
    }

    public void setNewData(List<Grade> gradeList) {
        grades.clear();
        if (grades != null)
            grades.addAll(gradeList);

    }


    public void deleteStudent(int position) {
        grades.remove(position);
        notifyItemRemoved(position);
    }


    public interface GradesAdapterInterface {
        void onGradeClick(Grade grade, int position);

        void onGradeLongPress(View view, Grade followedStudent, int position);
    }

    class GradeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        TextView subjectNameTextView;
        TextView examDescriptionTextView;
        BootstrapLabel examScore;


        public GradeViewHolder(View itemView) {
            super(itemView);
            examScore = (BootstrapLabel) itemView.findViewById(R.id.grade_item_score);
            subjectNameTextView = (TextView) itemView.findViewById(R.id.grade_item_subject_name);
            examDescriptionTextView = (TextView) itemView.findViewById(R.id.grade_item_exam_date);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {
            final int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                gradesAdapterInterface.onGradeClick(grades.get(position), position);
            }
        }

        @Override
        public boolean onLongClick(View v) {
            final int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                gradesAdapterInterface.onGradeLongPress(v, grades.get(position), position);
            }
            return true;
        }
    }
}
