package com.nocompany.mo7ammed.schoolityparent.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class TimetableDay implements Parcelable {
    public static final Creator<TimetableDay> CREATOR = new Creator<TimetableDay>() {
        @Override
        public TimetableDay createFromParcel(Parcel in) {
            return new TimetableDay(in);
        }

        @Override
        public TimetableDay[] newArray(int size) {
            return new TimetableDay[size];
        }
    };
    private String dayName;
    private List<Period> periods;

    public TimetableDay() {
        periods = new ArrayList<>();
    }

    public TimetableDay(String dayName, List<Period> periods) {
        this.dayName = dayName;
        this.periods = periods;
    }

    protected TimetableDay(Parcel in) {
        dayName = in.readString();
        periods = in.createTypedArrayList(Period.CREATOR);
    }

    public String getDayName() {
        return dayName;
    }

    public void setDayName(String dayName) {
        this.dayName = dayName;
    }

    public List<Period> getPeriods() {
        return periods;
    }

    public void setPeriods(List<Period> periods) {
        this.periods = periods;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(dayName);
        dest.writeTypedList(periods);
    }

    @Override
    public String toString() {
        return "TimetableDay{" +
                "dayName='" + dayName + '\'' +
                ", periods=" + periods +
                '}';
    }
}
