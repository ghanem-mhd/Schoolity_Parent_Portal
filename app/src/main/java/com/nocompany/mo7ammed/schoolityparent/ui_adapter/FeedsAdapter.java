package com.nocompany.mo7ammed.schoolityparent.ui_adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.BootstrapLabel;
import com.nocompany.mo7ammed.schoolityparent.R;
import com.nocompany.mo7ammed.schoolityparent.model.Feedable;

import java.util.ArrayList;
import java.util.List;

public class FeedsAdapter extends RecyclerView.Adapter<FeedsAdapter.FeedsViewHolder>{
    private Context context;
    private List<Feedable> feedables;
    private FeedsAdapterInterface feedsAdapterInterface;

    public FeedsAdapter(Context context) {
        this.context    = context;
        this.feedables  = new ArrayList<>();
    }

    public void setFeedsAdapterInterface(FeedsAdapterInterface feedsAdapterInterface) {
        this.feedsAdapterInterface = feedsAdapterInterface;
    }

    @Override
    public FeedsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.feed_item, parent, false);
        return new FeedsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(FeedsViewHolder holder, int position) {
        Feedable feedable = feedables.get(position);

        holder.titleTextView.setText(feedable.getTitle(context));
        holder.typeTextView.setText(feedable.getType(context));
        holder.summaryTextView.setText(feedable.getSummary(context));
    }

    @Override
    public int getItemCount() {
        return feedables.size();
    }

    public void setNewData(List<Feedable> feedableList) {
        feedables.clear();
        feedables.addAll(feedableList);
        notifyDataSetChanged();
    }

    public Integer appendData(List<Feedable> feedableList) {
        int numberOfNewItems = feedableList.size();
        int positionStart = feedables.size() + 1;
        feedables.addAll(feedableList);
        notifyItemRangeInserted(positionStart, numberOfNewItems);
        return numberOfNewItems;
    }

    class FeedsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        TextView titleTextView;
        TextView summaryTextView;
        BootstrapLabel typeTextView;

        public FeedsViewHolder(View itemView) {
            super(itemView);

            titleTextView       = (TextView) itemView.findViewById(R.id.feed_item_title);
            summaryTextView     = (TextView) itemView.findViewById(R.id.feed_item_summary);
            typeTextView        = (BootstrapLabel) itemView.findViewById(R.id.feed_item_type);


            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            final int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION && feedsAdapterInterface != null) {
                feedsAdapterInterface.onFeedsItemClick(feedables.get(position), position);
            }
        }

        @Override
        public boolean onLongClick(View v) {
            final int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION && feedsAdapterInterface != null) {
                feedsAdapterInterface.onFeedsItemLongPress(v, feedables.get(position), position);
            }
            return true;
        }
    }

    public interface FeedsAdapterInterface{
        void onFeedsItemClick(Feedable feedable,int position);
        void onFeedsItemLongPress(View view,Feedable feedable,int position);
    }
}
