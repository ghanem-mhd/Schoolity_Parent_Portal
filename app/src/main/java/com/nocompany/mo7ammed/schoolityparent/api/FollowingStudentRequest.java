package com.nocompany.mo7ammed.schoolityparent.api;

import android.content.Context;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.nocompany.mo7ammed.schoolityparent.BuildConfig;
import com.nocompany.mo7ammed.schoolityparent.db_cache.JSONsDatabaseHelper;
import com.nocompany.mo7ammed.schoolityparent.model.JSON;

import org.json.JSONArray;


public abstract class FollowingStudentRequest extends Request {
    public static final String URI = Request.REST_SERVER + "parent/followings";
    private static final String TAG = "FollowingStudentRequest";
    private static final int METHOD = Request.GET;
    JSONsDatabaseHelper jsoNsDatabaseHelper;

    private Response.Listener<JSONArray> jsonArrayListener = new Response.Listener<JSONArray>() {
        @Override
        public void onResponse(JSONArray response) {
            jsoNsDatabaseHelper.addJSON(new JSON(URI, response.toString()));
            FollowingStudentRequest.this.doOnResponse(response);
        }
    };

    private Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            FollowingStudentRequest.this.doOnError(error);
        }
    };


    public FollowingStudentRequest(Context context) {
        this.isAuthRequest = true;
        conHandler = new VolleyHandler(context);
        jsoNsDatabaseHelper = JSONsDatabaseHelper.getInstance();
    }

    @Override
    public void request(boolean withCaching) {

        if (withCaching) {
            checkCache();
        }

        CustomJsonArrayRequest customJsonObjectRequest = new CustomJsonArrayRequest(
                METHOD,
                URI,
                null,
                jsonArrayListener,
                errorListener,
                isAuthRequest);

        if (BuildConfig.DEBUG)
            Log.d(TAG, "making a request Method:" + METHOD + " URI:" + URI);

        conHandler.connect(customJsonObjectRequest);
    }

    private void checkCache() {
        JSON json = jsoNsDatabaseHelper.getJSON(URI);
        if (json != null) {
            FollowingStudentRequest.this.doOnResponse(json.getJsonArray());
        } else {
            if (BuildConfig.DEBUG)
                Log.d(TAG, " Following Students list Not Found in cache");
        }
    }

    protected abstract void doOnResponse(JSONArray jsonArray);

}
