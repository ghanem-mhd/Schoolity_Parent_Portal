package com.nocompany.mo7ammed.schoolityparent.api;


import android.content.Context;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.nocompany.mo7ammed.schoolityparent.BuildConfig;
import com.nocompany.mo7ammed.schoolityparent.model.SignUpObject;

import org.json.JSONException;
import org.json.JSONObject;

public abstract class SignUpRequest extends Request {
    private static final String TAG = "SignUpRequest";
    private static final String URI = Request.REST_SERVER + "sign_up";
    private static final int METHOD = Request.POST;
    private SignUpObject signUpObject;
    private JSONObject requestBody;

    private Response.Listener<JSONObject> jsonObjectListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            SignUpRequest.this.doOnResponse(response);
        }
    };
    private Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            SignUpRequest.this.doOnError(error);
        }
    };

    public SignUpRequest(Context context, SignUpObject signUpObject) {
        this.isAuthRequest = false;
        this.signUpObject = signUpObject;
        conHandler = new VolleyHandler(context);
        requestBody = getJsonObjectBody();
    }

    @Override
    public void request(boolean withCaching) {
        CustomJsonObjectRequest customJsonObjectRequest = new CustomJsonObjectRequest(
                METHOD,
                URI,
                requestBody,
                jsonObjectListener,
                errorListener,
                false
        );
        if (BuildConfig.DEBUG)
            Log.d(TAG, "making a request Method:" + METHOD + " URI:" + URI + " BODY:" + requestBody);
        conHandler.connect(customJsonObjectRequest);
    }

    public JSONObject getJsonObjectBody() {
        Gson gson = new Gson();
        try {
            return new JSONObject(gson.toJson(new SignUpReqBody(signUpObject)));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected abstract void doOnResponse(JSONObject jsonObject);

    class SignUpReqBody {
        SignUpObject user;
        public SignUpReqBody(SignUpObject signUpObject) {
            this.user = signUpObject;
        }
    }
}
