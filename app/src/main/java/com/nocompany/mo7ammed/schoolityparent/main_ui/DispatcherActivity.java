package com.nocompany.mo7ammed.schoolityparent.main_ui;


import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;

import com.nocompany.mo7ammed.schoolityparent.reg_ui.SignInActivity;
import com.nocompany.mo7ammed.schoolityparent.utilities.BaseActivity;
import com.nocompany.mo7ammed.schoolityparent.utilities.Constant;
import com.nocompany.mo7ammed.schoolityparent.utilities.PrefsKeys;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.Locale;

public class DispatcherActivity extends BaseActivity {

    public static final String CHANGING_LANGUAGE = "changing_language";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        changeApplicationLanguage();
        runDispatcher();
    }

    private void runDispatcher() {
        if (Prefs.getBoolean(PrefsKeys.IS_LOGIN,false)){
            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }else {
            Intent intent = new Intent(this, SignInActivity.class);
            startActivity(intent);
        }
        finish();
    }

    private void changeApplicationLanguage() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        String langID = sharedPref.getString(SettingsFragment.LANGUAGE, "0");
        Locale locale = null;
        if (langID.equals(Constant.ENGLISH))
            locale = new Locale("en");
        else
            locale = new Locale("ar");
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }
}