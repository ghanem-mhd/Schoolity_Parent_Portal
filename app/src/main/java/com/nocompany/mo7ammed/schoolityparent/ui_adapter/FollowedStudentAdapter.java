package com.nocompany.mo7ammed.schoolityparent.ui_adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nocompany.mo7ammed.schoolityparent.R;
import com.nocompany.mo7ammed.schoolityparent.model.FollowedStudent;

import java.util.ArrayList;
import java.util.List;

public class FollowedStudentAdapter extends RecyclerView.Adapter<FollowedStudentAdapter.FollowedStudentViewHolder> {

    private ArrayList<FollowedStudent> followedStudents;
    private FollowedStudentInterface followedStudentInterface;


    public FollowedStudentAdapter() {
        followedStudents = new ArrayList<>();
    }

    public void setFollowedStudentInterface(FollowedStudentInterface followedStudentInterface) {
        this.followedStudentInterface = followedStudentInterface;
    }

    @Override
    public FollowedStudentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.followed_student_item, parent, false);
        return new FollowedStudentViewHolder(v);
    }

    @Override
    public void onBindViewHolder(FollowedStudentViewHolder holder, int position) {
        FollowedStudent followedStudent = followedStudents.get(position);

        holder.studentName.setText(followedStudent.getStudentName());
        holder.studentRelationship.setText(String.format(holder.studentRelationship.getText().toString(), followedStudent.getRelationship()));
    }

    @Override
    public int getItemCount() {
        if (followedStudents != null)
            return followedStudents.size();
        return 0;
    }

    public void setNewData(List<FollowedStudent> newFollowedStudents) {
        followedStudents.clear();
        if (newFollowedStudents != null)
            followedStudents.addAll(newFollowedStudents);
    }


    public void deleteStudent(int position) {
        followedStudents.remove(position);
        notifyItemRemoved(position);
    }

    public interface FollowedStudentInterface {

        void onStudentClick(FollowedStudent followedStudent, int position);

        void onStudentLongPress(View view, FollowedStudent followedStudent, int position);
    }

    class FollowedStudentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        TextView studentName;
        TextView studentRelationship;


        public FollowedStudentViewHolder(View itemView) {
            super(itemView);
            studentName = (TextView) itemView.findViewById(R.id.followed_student_name);
            studentRelationship = (TextView) itemView.findViewById(R.id.followed_student_relationship);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {
            final int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                followedStudentInterface.onStudentClick(followedStudents.get(position), position);
            }
        }

        @Override
        public boolean onLongClick(View v) {
            final int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                followedStudentInterface.onStudentLongPress(v, followedStudents.get(position), position);
            }
            return true;
        }
    }
}




