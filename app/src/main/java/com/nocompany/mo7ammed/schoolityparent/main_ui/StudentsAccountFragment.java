package com.nocompany.mo7ammed.schoolityparent.main_ui;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.melnykov.fab.FloatingActionButton;
import com.nocompany.mo7ammed.schoolityparent.R;
import com.nocompany.mo7ammed.schoolityparent.api.FollowingStudentRequest;
import com.nocompany.mo7ammed.schoolityparent.api.UnFollowStudentRequest;
import com.nocompany.mo7ammed.schoolityparent.api.VolleyHandler;
import com.nocompany.mo7ammed.schoolityparent.helper.SimpleDividerItemDecoration;
import com.nocompany.mo7ammed.schoolityparent.model.Error;
import com.nocompany.mo7ammed.schoolityparent.model.FollowedStudent;
import com.nocompany.mo7ammed.schoolityparent.model.JSONParser;
import com.nocompany.mo7ammed.schoolityparent.ui_adapter.FollowedStudentAdapter;
import com.nocompany.mo7ammed.schoolityparent.utilities.BaseActivity;
import com.nocompany.mo7ammed.schoolityparent.utilities.BaseFragment;

import org.json.JSONArray;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StudentsAccountFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, FollowedStudentAdapter.FollowedStudentInterface, View.OnClickListener {

    private static final int FOLLOW_STUDENT_REQ = 1;

    @BindView(R.id.students_account_recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.students_account_refreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.students_account_error_textView)
    TextView errorTextView;
    @BindView(R.id.students_account_error_scrollView)
    ScrollView errorScrollView;
    @BindView(R.id.students_account_fab)
    FloatingActionButton floatingActionButton;

    private FollowedStudentAdapter followedStudentAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.students_account_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setToolbarTitle();

        setUpAdsList();

        API_FollowedStudent_Request(true);

        floatingActionButton.setOnClickListener(this);
    }

    private void setUpAdsList() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        followedStudentAdapter = new FollowedStudentAdapter();

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(followedStudentAdapter);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity().getApplicationContext()));

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getActivity(), R.color.colorAccent));

        followedStudentAdapter.setFollowedStudentInterface(this);

        floatingActionButton.attachToRecyclerView(recyclerView);
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                API_FollowedStudent_Request(false);
            }
        }, 500);
    }

    @Override
    public void onStudentClick(FollowedStudent followedStudent, int position) {
    }

    @Override
    public void onStudentLongPress(View view, final FollowedStudent followedStudent, int position) {
        PopupMenu popup = new PopupMenu(getActivity(), view);
        popup.getMenuInflater().inflate(R.menu.unfollow_student_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                API_UnFollow_Request(followedStudent.getFollowingID());
                return true;
            }
        });
        popup.show();
    }

    private void setRefreshing(final boolean isRefreshing) {
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(isRefreshing);
            }
        });
    }

    private void API_FollowedStudent_Request(boolean withCaching) {
        setRefreshing(!withCaching);
        FollowingStudentRequest followingStudentRequest = new FollowingStudentRequest(getActivity().getApplicationContext()) {
            @Override
            protected void doOnResponse(JSONArray jsonArray) {
                API_FollowedStudent_Response(jsonArray);
            }

            @Override
            protected void doOnError(VolleyError error) {
                VolleyHandler.handleVolleyError(error);
                API_FollowedStudent_Error(error);
            }
        };
        followingStudentRequest.request(withCaching);
    }

    private void API_FollowedStudent_Response(JSONArray jsonArray) {
        setRefreshing(false);
        List<FollowedStudent> followedStudents = JSONParser.getFollowedStudentsFromJsonArray(jsonArray);
        if (followedStudents != null && followedStudents.size() > 0) {

            errorScrollView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);

            followedStudentAdapter.setNewData(followedStudents);
            recyclerView.swapAdapter(followedStudentAdapter, true);

        } else {

            errorScrollView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
    }

    private void API_FollowedStudent_Error(VolleyError volleyError) {
        setRefreshing(false);
        On_API_Error(volleyError, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                API_FollowedStudent_Request(true);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.students_account_fab:
                openFollowStudentActivity();
                break;
        }
    }

    private void openFollowStudentActivity() {
        Intent intent = new Intent(getActivity(),FollowStudentActivity.class);
        startActivityForResult(intent,FOLLOW_STUDENT_REQ);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == FOLLOW_STUDENT_REQ){
            if (resultCode == BaseActivity.RESULT_OK){
                API_FollowedStudent_Request(false);
            }
        }
    }


    private void API_UnFollow_Request(int followingID) {
        setRefreshing(true);
        UnFollowStudentRequest unFollowStudentRequest = new UnFollowStudentRequest(getActivity(), followingID) {
            @Override
            protected void doOnResponse(String response) {
                API_UnFollow_Response(response);
            }

            @Override
            protected void doOnError(VolleyError error) {
                VolleyHandler.handleVolleyError(error);
                API_UnFollow_Error(new Error(StudentsAccountFragment.this.getActivity(), error));
            }
        };
        unFollowStudentRequest.request(false);
    }

    private void API_UnFollow_Response(String response) {
        setRefreshing(false);
        showLongToast(response);
        API_FollowedStudent_Request(false);
    }

    private void API_UnFollow_Error(Error error) {
        setRefreshing(false);
        showLongToast(error.getErrorMessage());
    }

    private void setToolbarTitle() {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.main_toolbar);
        toolbar.setTitle(getString(R.string.students_accounts));
    }
}
