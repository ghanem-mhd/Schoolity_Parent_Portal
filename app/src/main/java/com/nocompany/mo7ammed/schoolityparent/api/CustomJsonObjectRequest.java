package com.nocompany.mo7ammed.schoolityparent.api;


import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nocompany.mo7ammed.schoolityparent.model.UserInfo;
import com.nocompany.mo7ammed.schoolityparent.utilities.PrefsHelper;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class CustomJsonObjectRequest extends JsonObjectRequest {
    private boolean isAuth;

    public CustomJsonObjectRequest(int method, String url, JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener, boolean isAuth) {
        super(method, url, jsonRequest, listener, errorListener);
        this.isAuth = isAuth;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json");
        headers.put("Content-Type", "application/json");

        if (isAuth) {
            UserInfo userInfo = PrefsHelper.getUserInfo();
            headers.put("X-User-Email", userInfo.getEmail());
            headers.put("X-User-Token", userInfo.getAuthentication_token());
        }

        return headers;
    }
}
