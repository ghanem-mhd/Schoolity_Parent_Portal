package com.nocompany.mo7ammed.schoolityparent.utilities;

public abstract class PrefsKeys {
    public static final String IS_LOGIN = "is_login";
    public static final String USER_INFO = "user_info";
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";

}
