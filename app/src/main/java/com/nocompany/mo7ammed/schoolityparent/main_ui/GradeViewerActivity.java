package com.nocompany.mo7ammed.schoolityparent.main_ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.nocompany.mo7ammed.schoolityparent.R;
import com.nocompany.mo7ammed.schoolityparent.model.FollowedStudent;
import com.nocompany.mo7ammed.schoolityparent.model.Grade;
import com.nocompany.mo7ammed.schoolityparent.utilities.BaseActivity;
import com.nocompany.mo7ammed.schoolityparent.utilities.Constant;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GradeViewerActivity extends BaseActivity {

    public static final String Grade_OBJECT_ARG = "grade_arg";
    @BindView(R.id.exam_viewer_subject_name)
    TextView subjectName;
    @BindView(R.id.exam_viewer_subject_description)
    TextView subjectDescription;
    @BindView(R.id.exam_viewer_student_score)
    TextView studentScore;
    @BindView(R.id.exam_viewer_max_score)
    TextView maxScore;
    @BindView(R.id.exam_viewer_min_score)
    TextView minScore;
    @BindView(R.id.exam_viewer_student_name)
    TextView studentName;
    @BindView(R.id.exam_viewer_class_name)
    TextView className;
    @BindView(R.id.exam_viewer_classroom_name)
    TextView classRoomName;
    @BindView(R.id.exam_viewer_school_name)
    TextView schoolName;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private Grade grade;
    private FollowedStudent student;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.grade_viewer_activity);
        ButterKnife.bind(this);
        grade = (Grade) getIntent().getSerializableExtra(Grade_OBJECT_ARG);
        setUpToolbar();
        showExam();
    }

    private void setUpToolbar() {
        toolbar.setTitle(String.format("%s Grade", grade.getExam().getSubject().getName()));
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void showExam() {
        subjectName.setText(grade.getExam().getSubject().getName());
        if (!grade.getExam().getDescription().isEmpty()) {
            subjectDescription.setText(Html.fromHtml(grade.getExam().getDescription()));
        }
        studentScore.setText(grade.getScore());
        maxScore.setText(grade.getExam().getScore());
        minScore.setText(grade.getExam().getMinimum_score());
        className.setText(grade.getExam().getClassroom().getSchool_class());
        classRoomName.setText(grade.getExam().getClassroom().getName());
        schoolName.setText(grade.getExam().getClassroom().getSchool());
        studentName.setText(grade.getStudent());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(RESULT_CANCELED);
                finish();
                return true;

            case R.id.action_menu_discuss:
                Intent intent = new Intent(this,CommentsActivity.class);
                intent.putExtra(CommentsActivity.ID_ARG,grade.getId());
                intent.putExtra(CommentsActivity.TYPE_ARG, Constant.GRADE);
                intent.putExtra(CommentsActivity.TITLE_ARG,getString(R.string.grade));
                startActivity(intent);
                break;
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.discuss_menu, menu);
        return true;
    }
}
