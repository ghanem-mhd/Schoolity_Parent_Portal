package com.nocompany.mo7ammed.schoolityparent.main_ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.nocompany.mo7ammed.schoolityparent.R;
import com.nocompany.mo7ammed.schoolityparent.api.TimetableRequest;
import com.nocompany.mo7ammed.schoolityparent.api.VolleyHandler;
import com.nocompany.mo7ammed.schoolityparent.model.FollowedStudent;
import com.nocompany.mo7ammed.schoolityparent.model.JSONParser;
import com.nocompany.mo7ammed.schoolityparent.model.Timetable;
import com.nocompany.mo7ammed.schoolityparent.ui_adapter.TimetableAdapter;
import com.nocompany.mo7ammed.schoolityparent.utilities.BaseFragment;
import com.nocompany.mo7ammed.schoolityparent.utilities.StudentMenuManager;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TimetableFragment extends BaseFragment implements StudentMenuManager.StudentMenuInterface {

    @BindView(R.id.timetable_viewPager)
    ViewPager viewPager;

    @BindView(R.id.timetable_tabLayout)
    SmartTabLayout smartTabLayout;

    @BindView(R.id.timetable_error_textView)
    TextView errorTextView;

    private StudentMenuManager studentMenuManager;
    private FollowedStudent currentFollowedStudent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        studentMenuManager = new StudentMenuManager(this, getString(R.string.timetable));
        studentMenuManager.setStudentMenuInterface(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.timetable_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        studentMenuManager.getFollowingStudent();
    }

    private void API_Timetable_Request() {
        if (currentFollowedStudent == null){
            dismissLoadingDialog();
            return;
        }
        showLoadingDialog();
        TimetableRequest timetableRequest = new TimetableRequest(getActivity(), currentFollowedStudent.getFollowingID()) {
            @Override
            protected void doOnResponse(JSONObject jsonObject) {
                API_Timetable_Response(jsonObject);
            }

            @Override
            protected void doOnError(VolleyError error) {
                VolleyHandler.handleVolleyError(error);
                API_Timetable_Error(error);
            }
        };
        timetableRequest.request(true);
    }

    private void API_Timetable_Response(JSONObject jsonObject) {
        dismissLoadingDialog();
        removeErrorTextView();
        Timetable timetable = JSONParser.newTimetable(jsonObject);
        viewPager.setAdapter(new TimetableAdapter(getFragmentManager(), timetable.getTimetableDays()));
        smartTabLayout.setViewPager(viewPager);
    }

    private void API_Timetable_Error(VolleyError volleyError) {
        dismissLoadingDialog();
        On_API_Error(volleyError, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                API_Timetable_Request();
            }
        }, errorTextView);
    }

    private void removeErrorTextView() {
        errorTextView.setVisibility(View.GONE);
        viewPager.setVisibility(View.VISIBLE);
        smartTabLayout.setVisibility(View.VISIBLE);
    }

    private void showErrorTextView(String message) {
        viewPager.setVisibility(View.GONE);
        smartTabLayout.setVisibility(View.GONE);
        errorTextView.setVisibility(View.VISIBLE);
        errorTextView.setText(message);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        studentMenuManager.clearMenuAndSave();
    }

    @Override
    public void onChangeStudent(FollowedStudent followedStudent) {
        currentFollowedStudent = followedStudent;
        showErrorTextView("");
        API_Timetable_Request();
    }

    @Override
    public void onEmptyGetStudents() {
        setToolbarTitle();
        showErrorTextView(getString(R.string.no_following_student_go));
    }

    private void setToolbarTitle() {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.main_toolbar);
        toolbar.setTitle(getString(R.string.timetable));
    }
}
