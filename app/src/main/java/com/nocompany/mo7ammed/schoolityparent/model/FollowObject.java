package com.nocompany.mo7ammed.schoolityparent.model;


import com.nocompany.mo7ammed.schoolityparent.R;

public class FollowObject {

    private String code;
    private String full_name;
    private String relationship;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }


    public int checkData() {
        if (code == null || code.isEmpty()) {
            return R.string.check_code;
        }
        if (full_name == null || full_name.isEmpty()) {
            return R.string.check_name;
        }
        if (relationship == null || relationship.isEmpty()) {
            return R.string.check_relationship;
        }
        return -1;
    }
}
