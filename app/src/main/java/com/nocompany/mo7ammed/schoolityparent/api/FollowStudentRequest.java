package com.nocompany.mo7ammed.schoolityparent.api;


import android.content.Context;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.nocompany.mo7ammed.schoolityparent.BuildConfig;
import com.nocompany.mo7ammed.schoolityparent.model.FollowObject;

import org.json.JSONException;
import org.json.JSONObject;

public abstract class FollowStudentRequest extends Request {
    private static final String TAG = "FollowStudentRequest";
    private static final String URI = Request.REST_SERVER + "parent/followings";
    private static final int METHOD = Request.POST;
    private FollowObject followObject;
    private JSONObject requestBody;

    private Response.Listener<JSONObject> jsonObjectListener = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            FollowStudentRequest.this.doOnResponse(response);
        }
    };
    private Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            FollowStudentRequest.this.doOnError(error);
        }
    };

    public FollowStudentRequest(Context context, FollowObject followObject) {
        this.isAuthRequest = false;
        this.followObject = followObject;
        conHandler = new VolleyHandler(context);
        requestBody = getJsonObjectBody();
    }

    @Override
    public void request(boolean withCaching) {
        CustomJsonObjectRequest customJsonObjectRequest = new CustomJsonObjectRequest(
                METHOD,
                URI,
                requestBody,
                jsonObjectListener,
                errorListener,
                true
        );

        if (BuildConfig.DEBUG)
            Log.d(TAG, "making a request Method:" + METHOD + " URI:" + URI + " BODY:" + requestBody);
        conHandler.connect(customJsonObjectRequest);
    }

    private JSONObject getJsonObjectBody() {
        Gson gson = new Gson();
        try {
            return new JSONObject(gson.toJson(new FollowReqBody(followObject)));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected abstract void doOnResponse(JSONObject jsonObject);

    class FollowReqBody {
        FollowObject following;

        public FollowReqBody(FollowObject following) {
            this.following = following;
        }
    }
}
