package com.nocompany.mo7ammed.schoolityparent.gcm;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.google.android.gms.gcm.GcmListenerService;
import com.nocompany.mo7ammed.schoolityparent.R;
import com.nocompany.mo7ammed.schoolityparent.main_ui.MainActivity;
import com.nocompany.mo7ammed.schoolityparent.model.NotificationObject;
import com.nocompany.mo7ammed.schoolityparent.utilities.PrefsKeys;
import com.pixplicity.easyprefs.library.Prefs;

public class MyGcmListenerService extends GcmListenerService {

    private static final String TAG = "MyGcmListenerService";

    @Override
    public void onMessageReceived(String from, Bundle data) {
        NotificationObject notificationObject = new NotificationObject();
        notificationObject.setNotifiable(data.getString("notifiable"));
        notificationObject.setNotifiable_id(data.getString("notifiable_id"));
        notificationObject.setNotifiable_type(data.getString("notifiable_type"));
        notificationObject.setRole(data.getString("role"));
        notificationObject.setSummary(data.getString("summary"));
        showNotification(notificationObject);
    }


    private void showNotification(NotificationObject notificationObject) {

        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        bigTextStyle.setBigContentTitle(getString(R.string.app_name));
        bigTextStyle.bigText(notificationObject.getSummary()+".");

        if (Prefs.getBoolean(PrefsKeys.IS_LOGIN,false)){
            Intent intent = new Intent(this,MainActivity.class);
            intent.putExtra(MainActivity.NOTIFICATION_ARG,notificationObject);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, Integer.parseInt(notificationObject.getNotifiable_id()), intent, PendingIntent.FLAG_ONE_SHOT);

            NotificationCompat.Builder
                    notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.ic_notifications_blue_24dp)
                    .setAutoCancel(true)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setStyle(bigTextStyle)
                    .setContentIntent(pendingIntent);


            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(Integer.parseInt(notificationObject.getNotifiable_id()), notificationBuilder.build());

        }
    }
}
