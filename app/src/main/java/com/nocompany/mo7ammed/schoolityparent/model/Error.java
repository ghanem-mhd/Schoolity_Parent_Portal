package com.nocompany.mo7ammed.schoolityparent.model;

import android.content.Context;

import com.android.volley.VolleyError;
import com.nocompany.mo7ammed.schoolityparent.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Error {
    private Context context;
    private JSONArray jsonArray;

    public Error(Context context, VolleyError volleyError) {
        this.context = context;

        if (volleyError.networkResponse != null) {
            String data = new String(volleyError.networkResponse.data);
            try {
                JSONObject jsonObject = new JSONObject(data);
                jsonArray = jsonObject.getJSONArray("errors");
            } catch (JSONException e) {
                e.printStackTrace();
                jsonArray = null;
            }
        }
    }

    public String getErrorMessage() {
        if (jsonArray == null) {
            return context.getString(R.string.network_problem);
        } else {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    stringBuilder.append(jsonArray.getString(i));
                    if (i != jsonArray.length() - 1)
                        stringBuilder.append("\n");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return stringBuilder.toString();
        }
    }
}
