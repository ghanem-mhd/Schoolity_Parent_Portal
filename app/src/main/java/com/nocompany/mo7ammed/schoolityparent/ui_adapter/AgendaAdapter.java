package com.nocompany.mo7ammed.schoolityparent.ui_adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.nocompany.mo7ammed.schoolityparent.R;
import com.nocompany.mo7ammed.schoolityparent.model.Lesson;

import java.util.ArrayList;
import java.util.List;

public class AgendaAdapter extends RecyclerView.Adapter<AgendaAdapter.LessonViewHolder> {

    private Context context;
    private List<Lesson> lessons;
    private AgendaAdapterInterface agendaAdapterInterface;

    public AgendaAdapter(Context context) {
        this.context    = context;
        lessons         = new ArrayList<>();
    }

    public void setAgendaAdapterInterface(AgendaAdapterInterface agendaAdapterInterface) {
        this.agendaAdapterInterface = agendaAdapterInterface;
    }

    @Override
    public int getItemCount() {
        return lessons.size();
    }


    @Override
    public LessonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.lesson_item, viewGroup, false);
        return new LessonViewHolder(v);
    }

    @Override
    public void onBindViewHolder(LessonViewHolder lessonViewHolder, int i) {
        Lesson lesson = lessons.get(i);
        if (!lesson.getContent().getTitle().isEmpty()){
            lessonViewHolder.lessonTitle.setText(lesson.getContent().getTitle());
        }

        if (!lesson.getContent().getSummary().isEmpty()){
             lessonViewHolder.lessonSummary.setText(lesson.getContent().getSummary());
        }

        if (!lesson.getContent().getHomework().isEmpty()){
            lessonViewHolder.homeworkCheckBox.setChecked(true);
        }else {
            lessonViewHolder.homeworkCheckBox.setChecked(false);
        }

        if (lesson.getActivity() != null){
            lessonViewHolder.activityCheckBox.setChecked(true);
        }else {
            lessonViewHolder.activityCheckBox.setChecked(false);
        }

        if (lesson.getBehavior() != null){
            lessonViewHolder.behaviorCheckBox.setChecked(true);
        }else {
            lessonViewHolder.behaviorCheckBox.setChecked(false);
        }


        lessonViewHolder.lessonSubjectName.setText(lesson.getSubject());
    }

    public void setNewData(List<Lesson> newLessonList){
        lessons.clear();
        lessons.addAll(newLessonList);
        notifyDataSetChanged();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class LessonViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView lessonTitle;
        TextView lessonSummary;
        TextView lessonSubjectName;

        CheckBox homeworkCheckBox;
        CheckBox activityCheckBox;
        CheckBox behaviorCheckBox;

        LessonViewHolder(View itemView) {
            super(itemView);
            lessonTitle         = (TextView) itemView.findViewById(R.id.lesson_item_title);
            lessonSummary       = (TextView) itemView.findViewById(R.id.lesson_item_summary);
            lessonSubjectName   = (TextView) itemView.findViewById(R.id.lesson_item_subject);

            homeworkCheckBox    = (CheckBox) itemView.findViewById(R.id.lesson_item_homework_checkbox);
            activityCheckBox    = (CheckBox) itemView.findViewById(R.id.lesson_item_activity_checkbox);
            behaviorCheckBox    = (CheckBox) itemView.findViewById(R.id.lesson_item_behavior_checkbox);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            final int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION &&  agendaAdapterInterface!= null) {
                agendaAdapterInterface.onLessonClick(lessons.get(position), position);
            }
        }
    }

    public interface AgendaAdapterInterface{
        void onLessonClick(Lesson lesson,int position);
    }

}