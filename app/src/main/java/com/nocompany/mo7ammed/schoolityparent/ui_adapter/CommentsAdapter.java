package com.nocompany.mo7ammed.schoolityparent.ui_adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.curioustechizen.ago.RelativeTimeTextView;
import com.nocompany.mo7ammed.schoolityparent.R;
import com.nocompany.mo7ammed.schoolityparent.model.Comment;

import java.util.ArrayList;
import java.util.List;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.CommentsViewHolder> {

    private List<Comment> comments;
    private CommentsAdapterInterface commentsAdapterInterface;

    public CommentsAdapter() {
        comments = new ArrayList<>();
    }

    @Override
    public CommentsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_item, parent, false);
        return new CommentsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CommentsViewHolder holder, int position) {
        Comment comment = comments.get(position);

        String usernameAndRoleFormat = "%s As %s";
        if (comment.getOwner()) {
            holder.usernameTextView.setText("You");
        } else {
            holder.usernameTextView.setText(String.format(usernameAndRoleFormat, comment.getUser(), comment.getRole()));
        }
        holder.bodyTextView.setText(comment.getBody());
        holder.dateTextView.setReferenceTime(comment.getUpdateData().getTime());

    }

    public CommentsAdapterInterface getCommentsAdapterInterface() {
        return commentsAdapterInterface;
    }

    public void setCommentsAdapterInterface(CommentsAdapterInterface commentsAdapterInterface) {
        this.commentsAdapterInterface = commentsAdapterInterface;
    }

    @Override
    public int getItemCount() {
        return comments.size();
    }


    public void setNewData(List<Comment> commentsList) {
        comments.clear();
        if (commentsList != null)
            comments.addAll(commentsList);

    }


    public interface CommentsAdapterInterface {
        void onCommentClick(Comment comment, int position);

        void onCommentLongPress(View view, Comment comment, int position);
    }

    class CommentsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        TextView usernameTextView;
        TextView bodyTextView;
        RelativeTimeTextView dateTextView;

        public CommentsViewHolder(View itemView) {
            super(itemView);

            usernameTextView = (TextView) itemView.findViewById(R.id.comment_item_user_name);
            bodyTextView = (TextView) itemView.findViewById(R.id.comment_item_body);
            dateTextView = (RelativeTimeTextView) itemView.findViewById(R.id.comment_item_date);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {
            final int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                commentsAdapterInterface.onCommentClick(comments.get(position), position);
            }
        }

        @Override
        public boolean onLongClick(View v) {
            final int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                Comment comment = comments.get(position);
                if (comment.getOwner()) {
                    commentsAdapterInterface.onCommentLongPress(v, comments.get(position), position);
                }

            }
            return true;
        }
    }

}
