package com.nocompany.mo7ammed.schoolityparent.model;


import android.content.Context;

import com.nocompany.mo7ammed.schoolityparent.R;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Announcement implements Serializable,Feedable {
    public static final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

    private int id;
    private Object announceable;
    private int announceable_id;
    private String announceable_type;
    private int author_id;
    private String title;
    private String body;
    private String created_at;
    private String updated_at;
    private int comments_count;
    private String image_url;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAnnounceable_id() {
        return announceable_id;
    }

    public void setAnnounceable_id(int announceable_id) {
        this.announceable_id = announceable_id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getAuthor_id() {
        return author_id;
    }

    public void setAuthor_id(int author_id) {
        this.author_id = author_id;
    }

    public String getAnnounceable_type() {
        return announceable_type;
    }

    public void setAnnounceable_type(String announceable_type) {
        this.announceable_type = announceable_type;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    @Override
    public String toString() {
        return "Announcement{" +
                "id=" + id +
                ", announceable=" + announceable +
                ", announceable_id=" + announceable_id +
                ", announceable_type='" + announceable_type + '\'' +
                ", author_id=" + author_id +
                ", title='" + title + '\'' +
                ", body='" + body + '\'' +
                ", created_at='" + created_at + '\'' +
                ", updated_at='" + updated_at + '\'' +
                ", comments_count=" + comments_count +
                '}';
    }

    public static SimpleDateFormat getFormat() {
        return format;
    }

    public int getComments_count() {
        return comments_count;
    }

    public void setComments_count(int comments_count) {
        this.comments_count = comments_count;
    }

    public Date getCreateData() {
        try {
            return format.parse(created_at);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getTitle() {
        return title;
    }

    public Object getAnnounceable() {
        return announceable;
    }

    public void setAnnounceable(Object announceable) {
        this.announceable = announceable;
    }

    @Override
    public String getType(Context context) {
        return context.getString(R.string.announcement);
    }

    @Override
    public String getTitle(Context context) {
        return announceable_type +" "+context.getString(R.string.announcement);
    }

    @Override
    public String getSummary(Context context) {
        return title;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }
}
